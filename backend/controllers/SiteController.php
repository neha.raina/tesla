<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\LoginForm;
use backend\models\Admin;


/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error','forget', 'forget-password', 'reset-password'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'change-password'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
          /*  'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    { 
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    { 
        $dataProvider = Yii::$app->request->post();
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    { 
        if (!Yii::$app->user->isGuest) { 
            return $this->goHome();
        }
        
        $this->layout = 'blank';
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) { 
            return $this->goHome();
        } else { 
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
       
    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionForget()
    {
        $this->layout = 'blank';
        $model = new Admin();
        
        if ($model->load(Yii::$app->request->post()) && $model->forget()) {
          $message = $model->forget();
            return $this->render('forget-password', ['model' => $model, 'message' => $message]);
        } else {
            return $this->render('forget-password', ['model' => $model]);
        }
    }


    public function actionForgetPassword()
    {
      $this->layout = 'blank';
      $model = new Admin();

      $data = $model->checkResetToken(Yii::$app->request->get());
      if($data)
      {
         return $this->render('new-password', ['model' => $model, 'data' => $data]);
      }
      else
      {
        return $this->render('new-password', ['model' => $model]);
       
      }die;


    }

    public function actionResetPassword()
    {
        $this->layout = 'blank';
        $model = new Admin();
        if ($model->load(Yii::$app->request->post()) && $model->resetPassword()) {
          $data = $model->resetPassword();
            return $this->render('new-password', ['model' => $model, 'data' => $data]);
        } else {
            return $this->redirect('forget');
        }

    }

    public function actionChangePassword()
    {
        $model = new Admin();
        
        if ($model->load(Yii::$app->request->post()) && $model->changePassword()) {
          $data = $model->changePassword();
            return $this->render('change-password', ['model' => $model, 'data' => $data]);
        } else {
            return $this->render('change-password', ['model' => $model]);
        }
    }



}
