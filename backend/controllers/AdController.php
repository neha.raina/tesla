<?php

namespace backend\controllers;

use Yii;
use backend\models\Ad;
use backend\models\AdSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * AdController implements the CRUD actions for Ad model.
 */
class AdController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login','check-dinning','position','set-position','delete-ad','delete-video','delete-reservation-image'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['view', 'index','create','update','delete','check-dinning','position','set-position','delete-ad','delete-reservation-image'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Ad models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AdSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ad model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModelCustom($id),
        ]);
    }

    /**
     * Creates a new Ad model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ad();
        $model->active = '1';
        $model->adStatus = 'create';
        if ($model->load(Yii::$app->request->post()) && $model->insertAd('create')) {
            return $this->redirect(['view', 'id' => $model->pkAdID]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Ad model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModelCustom($id);
        $model->adStatus = 'update';
        if ($model->load(Yii::$app->request->post()) && $model->insertAd('update')) {
            return $this->redirect(['view', 'id' => $model->pkAdID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Ad model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ad model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Ad the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ad::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelCustom($id)
    {
        if (($model = Ad::find()->joinWith(['adMenus','fkCategory'])->where(['ad.pkAdID' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCheckDinning()
    {
        $values = Yii::$app->request->post();
        $model = new Ad();
        $response = $model->checkDinning($values);
        return $response;
    }

    public function actionDeleteAd()
    {
        
        $values = Yii::$app->request->post();
        $model = new Ad();
        $response = $model->deleteAd($values);
        return $response;
    }

    public function actionDeleteVideo()
    {
        $values = Yii::$app->request->post();
        $model = new Ad();
        $response = $model->deleteVideo($values);
        return $response;
    }

    public function actionDeleteReservationImage()
    {
        $values = Yii::$app->request->post();
        $model = new Ad();
        $response = $model->deleteReservationImage($values);
        return $response;
    }

    
    public function actionPosition()
    {
        $values = Yii::$app->request->post();
        if(isset($values['selectAdOrder']))
        {
            $id = $values['selectAdOrder'];
        }
        else
        {
            $id = NULL;
        }
        $model = new Ad();
        $response = $model->getAllAds($id);
        $response['formCategoryID'] = $id;
        return $this->render('position', [
                'model' => $response,
            ]);
    }

    public function actionSetPosition()
    {
        $values = Yii::$app->request->post();
        //print_r($values); die;
        $model = new Ad();
        $response = $model->setPosition($values);
        return $response;
    }


}
