<?php

namespace backend\models;

use Yii;
use yii\helpers\Url;
use backend\models\AdImage;
use backend\models\AdMenu;
use backend\models\CategoryAdOrder;

/**
 * This is the model class for table "ad".
 *
 * @property string $pkAdID
 * @property string $fkCategoryID
 * @property integer $position
 * @property integer $active
 * @property integer $dinning
 * @property string $title
 * @property string $subTitle
 * @property string $header
 * @property string $description
 * @property string $address
 * @property string $streetName
 * @property string $cityName
 * @property string $countryName
 * @property string $latitude
 * @property string $longitude
 * @property string $contactNumber
 * @property string $startDate
 * @property string $startDateTime
 * @property string $endDate
 * @property string $endDateTime
 * @property string $timestamp
 *
 * @property Category $fkCategory
 * @property AdImage[] $adImages
 * @property AdMenu[] $adMenus
 */
class Ad extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $catID;
    public $menuHeader;
    public $adStatus;
    public $deleteAdID;
    public $adImgType;
    public $positionCatId;
    public $positionArray;

    public static function tableName()
    {
        return 'ad';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkCategoryID', 'active', 'dinning'], 'integer'],
            [['fkCategoryID','title','address', 'cityName', 'countryName','description','header','contactNumber','startDate', 'startDateTime', 'endDate', 'endDateTime','latitude', 'longitude'], 'required'],
            [['title', 'subTitle', 'streetName', 'cityName', 'countryName'], 'string', 'max' => 100],
            [['header','videoPath', 'reservationImage'], 'string', 'max' => 200],
            [['catID','menuHeader','adStatus','deleteAdID','adImgType', 'latitude', 'longitude','positionArray','positionCatId'], 'safe'],
            [['description', 'address','reservationMessage'], 'string', 'max' => 1000],
            [['contactNumber', 'startDate', 'startDateTime', 'endDate', 'endDateTime'], 'string', 'max' => 20],
            [['fkCategoryID'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['fkCategoryID' => 'pkCategoryID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkAdID' => 'Pk Ad ID',
            'fkCategoryID' => 'Category',
            'active' => 'Active',
            'dinning' => 'Dinning',
            'title' => 'Title',
            'subTitle' => 'Sub Title',
            'header' => 'Header',
            'description' => 'Description',
            'address' => 'Address',
            'streetName' => 'Street Name',
            'cityName' => 'City Name',
            'countryName' => 'Country Name',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'contactNumber' => 'Contact Number',
            'videoPath' => 'Video Path',
            'reservationImage' => 'Reservation Image',
            'reservationMessage' => 'Reservation Message',
            'startDate' => 'Start Date',
            'startDateTime' => 'Start Date Time',
            'endDate' => 'End Date',
            'endDateTime' => 'End Date Time',
            'timestamp' => 'Timestamp',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkCategory()
    {
        return $this->hasOne(Category::className(), ['pkCategoryID' => 'fkCategoryID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdImages()
    {
        return $this->hasMany(AdImage::className(), ['fkAdID' => 'pkAdID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdMenus()
    {
        return $this->hasMany(AdMenu::className(), ['fkAdID' => 'pkAdID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryAdOrders()
    {
        return $this->hasMany(CategoryAdOrder::className(), ['fkAdID' => 'pkAdID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReservations()
    {
        return $this->hasMany(Reservation::className(), ['fkAdID' => 'pkAdID']);
    }
    

    public function checkDinning($params)
    {
        $this->load(array('Ad' => $params));
                
            $checkDinning = Category::find()->where(['dinning' => '1', 'pkCategoryID' => $this->catID])->one();
        
        if($checkDinning)
        {
            $arr['categoryName'] = $checkDinning->categoryName;
        }
        else
        {
            $arr['categoryName'] = '';
        }
        return json_encode($arr);
    }

    public function insertAd($param)
    {
          //echo '<pre>'; print_r($_FILES); echo '</pre>'; die;
          //echo '<pre>'; print_r($this); echo '</pre>'; die;
           
            $time = time();
            $this->timestamp = $time;

            // $address = trim($this->address);
            // $address = str_replace(" ", "+", $address);
            // $streetName = trim($this->streetName);
            // $streetName = str_replace(" ", "+", $streetName);
            // $cityName = trim($this->cityName);
            // $cityName = str_replace(" ", "+", $cityName);
            // $countryName = trim($this->countryName);
            // $countryName = str_replace(" ", "+", $countryName);
            
            // $myAddress = $address.','.$streetName.','.$cityName.','.$countryName;
            // $jsonAddress = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$myAddress&sensor=false");
            // $addressLatLong = json_decode($jsonAddress);

            // if(isset($addressLatLong->results[0]->geometry->location))
            // {
            // $latitude = (isset($addressLatLong->results[0]->geometry->location->lat)) ? $addressLatLong->results[0]->geometry->location->lat : NULL;
            // $longitude = (isset($addressLatLong->results[0]->geometry->location->lng)) ? $addressLatLong->results[0]->geometry->location->lng : NULL;

            // $this->latitude = $latitude;
            // $this->longitude = $longitude;
            // }

            $categoryID = $this->fkCategoryID;
            $dinningCategory = $this->dinning;
            $menuHeader = $this->menuHeader;
            
            if(@$_FILES['adVideo']['name'])
            {
                $adVideo = self::uploadVideo($_FILES['adVideo']);
                    if($adVideo['fileName'])
                    {
                        $this->videoPath = $adVideo['fileName'];
                    }
            }

            if(@$_FILES['reservationImage']['name'])
            {
                $reservationImage = self::uploadReservationImage($_FILES['reservationImage']);
                    if($reservationImage['fileName'])
                    {
                        $this->reservationImage = $reservationImage['fileName'];
                    }
            }

            if($this->save())
            {
                $adID  = $this->pkAdID;

                if($param == 'create')
                {
                    $position = CategoryAdOrder::find()->where(['fkCategoryID' => $categoryID])->max('position');    
                    $addPosition = new CategoryAdOrder();
                    $addPosition->fkAdID = $adID;
                    $addPosition->fkCategoryID = $categoryID;
                    $addPosition->position = $position + 1;

                    $addPosition->save();
                }

                if($param == 'update')
                {
                    $update = AdImage::updateAll(['fkCategoryID' => $categoryID], 'fkAdID = '.$adID);
                }

                if($dinningCategory == '1')
                {
                    if(@$_FILES['menuPDF']['name'])
                    {
                        $menuPDF = self::uploadPDF($_FILES['menuPDF']);
                        if($menuPDF['fileName'])
                        {
                            $pdfPath = $menuPDF['fileName'];

                            if($param == 'create')
                            {
                                $AdMenu = new AdMenu();    
                            }
                            else
                            {
                                $AdMenu = AdMenu::find()->where(['fkAdID' => $adID, 'fkCategoryID' => $categoryID])->one();
                                if(!$AdMenu)
                                {
                                    $AdMenu = new AdMenu();
                                }
                            }
                            
                            $AdMenu->menuPdfPath = $pdfPath;
                            $AdMenu->fkAdID = $adID;
                            $AdMenu->fkCategoryID = $categoryID;
                            $AdMenu->save();

                        }
                    }

                    if($param == 'update')
                    {
                        $AdMenu = AdMenu::find()->where(['fkAdID' => $adID, 'fkCategoryID' => $categoryID])->one();
                        if($AdMenu)
                        {

                            $AdMenu->fkAdID = $adID;
                            $AdMenu->fkCategoryID = $categoryID;
                            $AdMenu->menuHeader = $menuHeader;
                            $AdMenu->save();
                        }
                    }
                }
                

                if(@$_FILES['splashImage']['name'])
                {
                    $splashImage = self::uploadImage($_FILES['splashImage']);
                        if($splashImage['fileName'])
                        {
                            $imagePath = $splashImage['fileName'];

                            if($param == 'create')
                            {
                                $AdImage = new AdImage();    
                            }
                            else
                            {
                                $AdImage = AdImage::find()->where(['fkAdID' => $adID, 'imageType' => 'splashImage' ])->one();
                                if(!$AdImage)
                                {
                                    $AdImage = new AdImage();
                                }
                            }
                            
                            $AdImage->fkAdID = $adID;
                            $AdImage->fkCategoryID = $categoryID;
                            $AdImage->imageType = 'splashImage';
                            $AdImage->imagePath = $imagePath;

                            $AdImage->save();
                        }
                }

                if(@$_FILES['mainImage']['name'])
                {
                    $mainImage = self::uploadImage($_FILES['mainImage']);
                        if($mainImage['fileName'])
                        {
                            $imagePath = $mainImage['fileName'];
                            if($param == 'create')
                            {
                                $AdImage = new AdImage();    
                            }
                            else
                            {
                                $AdImage = AdImage::find()->where(['fkAdID' => $adID, 'imageType' => 'mainImage' ])->one();
                                if(!$AdImage)
                                {
                                    $AdImage = new AdImage();
                                }
                            }
                            $AdImage->fkAdID = $adID;
                            $AdImage->fkCategoryID = $categoryID;
                            $AdImage->imageType = 'mainImage';
                            $AdImage->imagePath = $imagePath;

                            $AdImage->save();
                            
                            $adInfo = self::find()->where(['pkAdID' => $adID])->one();

                            $myBaseUrl = Url::base(true);
                            $imageUrl = $myBaseUrl.'/ads/'.$imagePath;
                            $size = getimagesize($imageUrl);
                            $adInfo->mainImageWidth = $size[0];
                            $adInfo->mainImageHeight = $size[1];

                            $adInfo->save();
                        }
             
                        

                }

                if(@$_FILES['leftImage']['name'])
                {
                    $leftImage = self::uploadImage($_FILES['leftImage']);
                        if($leftImage['fileName'])
                        {
                            $imagePath = $leftImage['fileName'];
                            if($param == 'create')
                            {
                                $AdImage = new AdImage();    
                            }
                            else
                            {
                                $AdImage = AdImage::find()->where(['fkAdID' => $adID, 'imageType' => 'leftImage' ])->one();
                                if(!$AdImage)
                                {
                                    $AdImage = new AdImage();
                                }
                            }
                            $AdImage->fkAdID = $adID;
                            $AdImage->fkCategoryID = $categoryID;
                            $AdImage->imageType = 'leftImage';
                            $AdImage->imagePath = $imagePath;

                            $AdImage->save();
                        }
                }

                if(@$_FILES['rightImage']['name'])
                {
                    $rightImage = self::uploadImage($_FILES['rightImage']);
                        if($rightImage['fileName'])
                        {
                            $imagePath = $rightImage['fileName'];
                            if($param == 'create')
                            {
                                $AdImage = new AdImage();    
                            }
                            else
                            {
                                $AdImage = AdImage::find()->where(['fkAdID' => $adID, 'imageType' => 'rightImage' ])->one();
                                if(!$AdImage)
                                {
                                    $AdImage = new AdImage();
                                }
                            }
                            $AdImage->fkAdID = $adID;
                            $AdImage->fkCategoryID = $categoryID;
                            $AdImage->imageType = 'rightImage';
                            $AdImage->imagePath = $imagePath;

                            $AdImage->save();
                        }
                }

                return true;
            
            }
            else
            {
                //$errors = $this->getErrors();
                return false;
            }
    }

    public static function uploadVideo($params)
    {
        if(isset($params) && $params['size'] > 0)
        {
            $arrMediaInfo = pathinfo($params['name']);
            $extension = $arrMediaInfo['extension'];
            $rand = Yii::$app->security->generateRandomString();
            $uploadDir = Url::to('@backend').'/web/ads/videos'; 
    
            $arrMediaInfo['filename'] = str_replace(' ', '', $arrMediaInfo['filename']);
            $varFileNameWithTimeStamp = $arrMediaInfo['filename'].'-'.$rand;
            $newFileName = $uploadDir.'/'.$varFileNameWithTimeStamp.'.'.$extension;

            if(move_uploaded_file($params['tmp_name'], $newFileName))
            {
                    $data['fileName'] = $varFileNameWithTimeStamp.'.'.$extension;
                    return $data;
            }
            return false;
        }
    }


    public static function uploadReservationImage($params)
    {
        if(isset($params) && $params['size'] > 0)
        {
            $arrMediaInfo = pathinfo($params['name']);
            $extension = $arrMediaInfo['extension'];
            $rand = Yii::$app->security->generateRandomString();
            $uploadDir = Url::to('@backend').'/web/ads/reservation'; 
    
            $arrMediaInfo['filename'] = str_replace(' ', '', $arrMediaInfo['filename']);
            $varFileNameWithTimeStamp = $arrMediaInfo['filename'].'-'.$rand;
            $newFileName = $uploadDir.'/'.$varFileNameWithTimeStamp.'.'.$extension;

            if(move_uploaded_file($params['tmp_name'], $newFileName))
            {
                    $data['fileName'] = $varFileNameWithTimeStamp.'.'.$extension;
                    return $data;
            }
            return false;
        }
    }

    public static function uploadImage($params)
    {
        if(isset($params) && $params['size'] > 0)
        {
            $arrMediaInfo = pathinfo($params['name']);
            $extension = $arrMediaInfo['extension'];
            $rand = Yii::$app->security->generateRandomString();
            $uploadDir = Url::to('@backend').'/web/ads'; 
    
            $arrMediaInfo['filename'] = str_replace(' ', '', $arrMediaInfo['filename']);
            $varFileNameWithTimeStamp = $arrMediaInfo['filename'].'-'.$rand;
            $newFileName = $uploadDir.'/'.$varFileNameWithTimeStamp.'.'.$extension;

            if(move_uploaded_file($params['tmp_name'], $newFileName))
            {
                    $data['fileName'] = $varFileNameWithTimeStamp.'.'.$extension;
                    //$data['fileUrl'] = Url::base(true).'/restaurant/stores/'.$varFileNameWithTimeStamp.'.'.$extension;
                    return $data;
            }
            return false;
        }
    }

    public static function uploadPDF($params)
    {
        if(isset($params) && $params['size'] > 0)
        {
            $arrMediaInfo = pathinfo($params['name']);
            $extension = $arrMediaInfo['extension'];
            $rand = Yii::$app->security->generateRandomString();
            $uploadDir = Url::to('@backend').'/web/pdf'; 
    
            $arrMediaInfo['filename'] = str_replace(' ', '', $arrMediaInfo['filename']);
            $varFileNameWithTimeStamp = $arrMediaInfo['filename'].'-'.$rand;
            $newFileName = $uploadDir.'/'.$varFileNameWithTimeStamp.'.'.$extension;

            if(move_uploaded_file($params['tmp_name'], $newFileName))
            {
                    $data['fileName'] = $varFileNameWithTimeStamp.'.'.$extension;
                    //$data['fileUrl'] = Url::base(true).'/restaurant/stores/'.$varFileNameWithTimeStamp.'.'.$extension;
                    return $data;
            }
            return false;
        }
    }

    public static function deleteAd($params)
    {
        //return json_encode(getcwd());
        //$this->load(array('Category' => $params));
        $result = AdImage::find()->where(['fkAdID' => $params['deleteAdID'] , 'imageType' => $params['adImgType']])->one();
        if($result)
        {  
            $imagePath = $result->imagePath;
            $imageUrl = getcwd().'/ads/'.$imagePath;
            unlink($imageUrl);
            if($result->delete()){

                if($params['adImgType'] == 'mainImage')
                {

                    $adInfo = self::find()->where(['pkAdID' => $params['deleteAdID']])->one();
                    $adInfo->mainImageWidth = NULL;
                    $adInfo->mainImageHeight = NULL;

                    $adInfo->save(); 

                }

                return json_encode('deleted');
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }

    }

    public static function deleteVideo($params)
    {
        $result = self::find()->where(['pkAdID' => $params['deleteAdID']])->one();
        if($result)
        {   
            $videoPath = $result->videoPath;
            $result->videoPath = NULL;
            $videoUrl = getcwd().'/ads/videos/'.$videoPath;
            unlink($videoUrl);
            if($result->save()){
                return json_encode('deleted');
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }

    }

    public static function deleteReservationImage($params)
    {
        $result = self::find()->where(['pkAdID' => $params['deleteAdID']])->one();
        if($result)
        {   
            $reservationImagePath = $result->reservationImage;
            $result->reservationImage = NULL;
            $imageUrl = getcwd().'/ads/reservation/'.$reservationImagePath;
            unlink($imageUrl);
            if($result->save()){
                return json_encode('deleted');
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }

    }

    public static function getAllAds($id)
    {

        $ads = self::find()->where(['ad.fkCategoryID' => $id, 'ad.active' => '1'])->select(['ad.pkAdID', 'ad.title'])->joinWith(['categoryAdOrders'])->orderBy(['categoryAdOrder.position' => SORT_ASC])->all();
        return $ads;
    }

    public static function setPosition($params)
    {
       if($params['positionArray'][0])
       {
        $x=1;
            foreach($params['positionArray'] as $paramsValue)
            {
                   
            $ad = categoryAdOrder::findOne(['fkAdID' => $paramsValue, 'fkCategoryID' => $params['positionCatId']]);
                if($ad)
                {
                    $ad->position = $x;
                    $ad->save();
                }
                $x++;
            }
        }
         return true;
    }

}
