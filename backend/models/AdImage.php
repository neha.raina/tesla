<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ad_image".
 *
 * @property string $pkAdImageID
 * @property string $fkAdID
 * @property string $fkCategoryID
 * @property string $imageType
 * @property string $imagePath
 *
 * @property Ad $fkAd
 * @property Category $fkCategory
 */
class AdImage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ad_image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkAdID', 'fkCategoryID'], 'integer'],
            [['imageType'], 'string', 'max' => 20],
            [['imagePath'], 'string', 'max' => 500],
            [['fkAdID'], 'exist', 'skipOnError' => true, 'targetClass' => Ad::className(), 'targetAttribute' => ['fkAdID' => 'pkAdID']],
            [['fkCategoryID'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['fkCategoryID' => 'pkCategoryID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkAdImageID' => 'Pk Ad Image ID',
            'fkAdID' => 'Fk Ad ID',
            'fkCategoryID' => 'Fk Category ID',
            'imageType' => 'Image Type',
            'imagePath' => 'Image Path',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkAd()
    {
        return $this->hasOne(Ad::className(), ['pkAdID' => 'fkAdID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkCategory()
    {
        return $this->hasOne(Category::className(), ['pkCategoryID' => 'fkCategoryID']);
    }
}
