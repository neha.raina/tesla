<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "feedback".
 *
 * @property string $pkFeedbackID
 * @property string $name
 * @property string $emailID
 * @property string $message
 * @property string $latitude
 * @property string $longitude
 * @property string $timestamp
 */
class Feedback extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 100],
            [['emailID'], 'string', 'max' => 200],
            [['message'], 'string', 'max' => 1000],
            [['latitude', 'longitude', 'timestamp'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkFeedbackID' => 'Pk Feedback ID',
            'name' => 'Name',
            'emailID' => 'Email ID',
            'message' => 'Message',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'timestamp' => 'Timestamp',
        ];
    }
}
