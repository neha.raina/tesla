<?php

namespace backend\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "admin".
 *
 * @property integer $pkAdminID
 * @property string $adminEmail
 * @property string $adminPassword
 * @property string $adminResetToken
 * @property string $auth_key
 */
class Admin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'admin';
    }
        public $adminID;
        public $oldPassword;
        public $newPassword;
        public $reNewPassword;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['adminEmail', 'adminResetToken', 'auth_key'], 'string', 'max' => 100],
            [['adminPassword'], 'string', 'max' => 150],
            [['adminEmail', 'oldPassword', 'newPassword', 'reNewPassword'], 'required'],
            [['adminID'], 'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkAdminID' => 'Pk Admin ID',
            'adminEmail' => 'Email',
            'adminPassword' => 'Admin Password',
            'adminResetToken' => 'Admin Reset Token',
            'auth_key' => 'Auth Key',
        ];
    }

    public function forget()
    {
        //echo '<pre>'; print_r($this->adminEmail);
        $adminEmail = (isset($this->adminEmail)) ? $this->adminEmail : NULL;

        if($adminEmail)
        {
            $checkEmail = self::find()->where(['adminEmail' => $adminEmail])->one();
            if($checkEmail)
            {
                $checkEmail->adminResetToken = Yii::$app->security->generateRandomString() . '_' . time();
                $myBaseUrl = Url::base(true);
               
                $link = $myBaseUrl.'/site/forget-password?uid='.$checkEmail->pkAdminID.'&token='.$checkEmail->adminResetToken;   
                

                if($checkEmail->update())
                {
                    $res = 'Hi!';
                    $res .= 'Your passowrd reset link is: ';
                    $res .= $link.'  ';
                    $res .= 'Click on the link.';
                    mail($adminEmail, 'Cabitall : Forget Password', $res);
                    //$message = 'Password reset link has been sent to your email id.';
                    $message = $link;
                }
            }
            else
            {
                $message = 'Email doesnot exists.';
            }
        }
        else
        {
            $message = 'Email cannot be empty.';
        }
        return $message;

    }

    public function checkResetToken($params)
    {    
        $adminId = (isset($params['uid'])) ? $params['uid'] : NULL;
        $resetToken = (isset($params['token'])) ? $params['token'] : NULL;

        if($adminId && $resetToken)
        {

            $check = self::find()->where(['pkAdminID' => $adminId , 'adminResetToken' => $resetToken])->one();
            if($check)
            {   
                $data['message'] = 'active';
                $data['adminID'] = $adminId;
            }
            else
            {
                $data['message'] = 'Reset Token doesn\'t exists.';    
            }
        }
        else
        {
            $data['message'] = 'Reset Token doesn\'t exists.';
        }

        return $data;
    }

    public function resetPassword()
    {

        //echo '<pre>'; print_r($this->adminID); die;
        $password = (isset($this->adminPassword)) ? $this->adminPassword : NULL;
        $adminId = (isset($this->adminID)) ? $this->adminID : NULL;

        if($adminId && $password)
        {

            $admin = self::find()->where(['pkAdminID' => $adminId])->one();
            if($admin)
            {
                $admin->adminResetToken = Yii::$app->security->generateRandomString() . '_' . time();
                $varPasswordHash = Yii::$app->getSecurity()->generatePasswordHash($password);

                $admin->adminPassword = $varPasswordHash;
                if($admin->update())
                {
                    $data['status'] = 'Changed';
                }
                else
                {
                    $data['status'] = 'Error';   
                }
            }
            else
            {
                $data['status'] = 'Error';
            }
        }
        else
        {
            $data['status'] = 'Error';
        }
         return $data;
    }

    public function changePassword()
    {
      $postvalues = Yii::$app->request->post();
      $this->load($postvalues);
      $currUserId = Yii::$app->user->getId();
      //echo '<pre>';print_r($this); die;

      if(($this->oldPassword) && ($this->newPassword))
      {

        $currentpass = Yii::$app->user->identity->adminPassword;
      
        if (!(Yii::$app->getSecurity()->validatePassword($this->oldPassword,$currentpass))) {
             $data['status'] = 'Error';
             $data['error'] = 'Old Password doesn\'t match.';
             return $data;
        }
        else
        {
            $adminInfo = self::find()->where(['pkAdminID' => $currUserId])->one();
            $adminInfo->load($postvalues);
            //echo '<pre>'; print_r($adminInfo); die;
            if($adminInfo)
            {   
                $adminInfo->adminResetToken = Yii::$app->security->generateRandomString() . '_' . time();
                $varPasswordHash = Yii::$app->getSecurity()->generatePasswordHash($this->newPassword);
                $adminInfo->adminPassword = $varPasswordHash;

                if($adminInfo->update())
                {
                    $data['status'] = 'Changed';
                }
                else
                {
                    $data['status'] = 'Error';
                    $data['error'] = 'Error coming while changing the password.';
                    return $data;       
                }
            }
            else
            {
                $data['status'] = 'Error';
                $data['error'] = 'User detail not found.';
                return $data;       
            }
        }

      }
      else
      {
        $data['status'] = 'Error';
        $data['error'] = 'Feilds cannot be empty';
      }
        return $data;

    }


}
