<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "reservation".
 *
 * @property string $pkReservationID
 * @property string $fkAdID
 * @property string $fkCategoryID
 * @property integer $confirmation
 * @property string $userName
 * @property string $userEmail
 * @property string $phoneNumber
 * @property string $bookingDate
 * @property string $bookingTime
 * @property integer $person
 * @property string $timestamp
 *
 * @property Category $fkCategory
 * @property Ad $fkAd
 */
class Reservation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $title;
    public static function tableName()
    {
        return 'reservation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkAdID', 'fkCategoryID', 'confirmation', 'person'], 'integer'],
            [['userName', 'bookingDate'], 'string', 'max' => 100],
            [['title'], 'safe'],
            [['userEmail', 'phoneNumber', 'bookingTime', 'timestamp'], 'string', 'max' => 20],
            [['fkCategoryID'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['fkCategoryID' => 'pkCategoryID']],
            [['fkAdID'], 'exist', 'skipOnError' => true, 'targetClass' => Ad::className(), 'targetAttribute' => ['fkAdID' => 'pkAdID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkReservationID' => 'Pk Reservation ID',
            'fkAdID' => 'Fk Ad ID',
            'fkCategoryID' => 'Fk Category ID',
            'confirmation' => 'Confirmation',
            'userName' => 'User Name',
            'userEmail' => 'User Email',
            'phoneNumber' => 'Phone Number',
            'bookingDate' => 'Booking Date',
            'bookingTime' => 'Booking Time',
            'person' => 'Person',
            'timestamp' => 'Timestamp',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkCategory()
    {
        return $this->hasOne(Category::className(), ['pkCategoryID' => 'fkCategoryID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkAd()
    {
        return $this->hasOne(Ad::className(), ['pkAdID' => 'fkAdID']);
    }
}
