<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "about_us".
 *
 * @property string $pkAboutUsID
 * @property string $header
 * @property string $description
 * @property string $startingRate
 * @property string $pricePerKm
 */
class AboutUs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'about_us';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['header', 'description'], 'string', 'max' => 1000],
            [['startingRate'], 'string', 'max' => 10],
            [['pricePerKm'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkAboutUsID' => 'Pk About Us ID',
            'header' => 'Header',
            'description' => 'Description',
            'startingRate' => 'Starting Rate',
            'pricePerKm' => 'Price Per Km',
        ];
    }
}
