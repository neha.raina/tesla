<?php

namespace backend\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "category".
 *
 * @property string $pkCategoryID
 * @property string $categoryName
 * @property integer $dinning
 * @property integer $active
 * @property integer $position
 * @property string $videoPath
 * @property string $timestamp
 *
 * @property Ad[] $ads
 * @property AdImage[] $adImages
 * @property AdMenu[] $adMenus
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $update;
    public $catID;
    public $positionArray;
    public $deleteCatID;
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dinning', 'active', 'position'], 'integer'],
            [['categoryName'], 'required'],
            [['timestamp','catID','positionArray','deleteCatID'], 'safe'],
            [['categoryName'], 'string', 'max' => 50],
            [['videoPath'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkCategoryID' => 'Pk Category ID',
            'categoryName' => 'Category Name',
            'dinning' => 'Dinning',
            'active' => 'Active',
            'position' => 'Position',
            'videoPath' => 'Video Path',
            'timestamp' => 'Timestamp',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAds()
    {
        return $this->hasMany(Ad::className(), ['fkCategoryID' => 'pkCategoryID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdImages()
    {
        return $this->hasMany(AdImage::className(), ['fkCategoryID' => 'pkCategoryID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdMenus()
    {
        return $this->hasMany(AdMenu::className(), ['fkCategoryID' => 'pkCategoryID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryAdOrders()
    {
        return $this->hasMany(CategoryAdOrder::className(), ['fkCategoryID' => 'pkCategoryID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReservations()
    {
        return $this->hasMany(Reservation::className(), ['fkCategoryID' => 'pkCategoryID']);
    }


    public function checkDinning($params)
    {
        $this->load(array('Category' => $params));
        if($this->catID)
        {
            $checkDinning = self::find()->where(['dinning' => '1'])->andWhere(['not',['pkCategoryID' => $this->catID]])->one();
        }
        else
        {
            $checkDinning = self::find()->where(['dinning' => '1'])->one();
        }
        
        if($checkDinning)
        {
            $arr['categoryName'] = $checkDinning->categoryName;
        }
        else
        {
            $arr['categoryName'] = '';
        }
        return json_encode($arr);
    }

    public function insertCategory($param)
    {
          // echo '<pre>'; print_r($_FILES); echo '</pre>';
          // echo '<pre>'; print_r($this); echo '</pre>'; die;
            if($param == 'create')
            {
                $position = self::find()->max('position');    
                $this->position = $position + 1;
            }
            
            $time = time();
            $this->timestamp = $time;
            
            if($this->save())
            {
                return true;
            }
            else
            {
                return false;
            }
    }

    

    public static function getAllCategories()
    {
        $categories = self::find()->where(['active' => '1'])->orderBy(['position' => SORT_ASC])->all();
        return $categories;
    }

    public static function setPosition($params)
    {
       if($params['positionArray'][0])
       {
        self::updateAll(['position' => NULL]);
        $x=1;
            foreach($params['positionArray'] as $paramsValue)
            {
                   
            $category = self::findOne(['pkCategoryID' => $paramsValue]);
                if($category)
                {
                    $category->position = $x;
                    $category->save();
                }
                $x++;
            }
        }
         return true;
    }
}
