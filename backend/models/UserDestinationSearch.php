<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\UserDestination;

/**
 * UserDestinationSearch represents the model behind the search form about `backend\models\UserDestination`.
 */
class UserDestinationSearch extends UserDestination
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pkUserDestinationID'], 'integer'],
            [['streetName', 'cityName', 'phoneNumber', 'timestamp'], 'safe'],
            [['latitude', 'longitude'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserDestination::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        // $query->andFilterWhere([
        //     'pkUserDestinationID' => $this->pkUserDestinationID,
        //     'latitude' => $this->latitude,
        //     'longitude' => $this->longitude,
        // ]);

        $query->andFilterWhere(['like', 'streetName', $this->streetName])
            ->andFilterWhere(['like', 'latitude', $this->latitude])
            ->andFilterWhere(['like', 'phoneNumber', $this->phoneNumber])
            ->andFilterWhere(['like', 'longitude', $this->longitude]);

        return $dataProvider;
    }
}
