<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Ad;

/**
 * AdSearch represents the model behind the search form about `backend\models\Ad`.
 */
class AdSearch extends Ad
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pkAdID', 'fkCategoryID', 'active', 'dinning'], 'integer'],
            [['title', 'subTitle', 'header', 'description', 'address', 'streetName', 'cityName', 'countryName', 'latitude', 'longitude', 'contactNumber', 'startDate', 'startDateTime', 'endDate', 'endDateTime', 'timestamp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ad::find()->joinWith(['adMenus','fkCategory','categoryAdOrders'])->orderBy(['category.position' => SORT_ASC, 'categoryAdOrder.position' => SORT_ASC]);

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            //'pkAdID' => $this->pkAdID,
            'ad.fkCategoryID' => $this->fkCategoryID,
            //'position' => $this->position,
            'ad.active' => $this->active,
            'ad.dinning' => $this->dinning,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);
            //->andFilterWhere(['like', 'subTitle', $this->subTitle])
            //->andFilterWhere(['like', 'header', $this->header])
            //->andFilterWhere(['like', 'description', $this->description])
            //->andFilterWhere(['like', 'address', $this->address])
            //->andFilterWhere(['like', 'streetName', $this->streetName])
            // ->andFilterWhere(['like', 'cityName', $this->cityName])
            // ->andFilterWhere(['like', 'countryName', $this->countryName])
            // ->andFilterWhere(['like', 'latitude', $this->latitude])
            //->andFilterWhere(['like', 'longitude', $this->longitude])
            // ->andFilterWhere(['like', 'contactNumber', $this->contactNumber])
            //->andFilterWhere(['like', 'startDate', $this->startDate])
            //->andFilterWhere(['like', 'startDateTime', $this->startDateTime])
            //->andFilterWhere(['like', 'endDate', $this->endDate])
            //->andFilterWhere(['like', 'endDateTime', $this->endDateTime])
            //->andFilterWhere(['like', 'timestamp', $this->timestamp]);

        return $dataProvider;
    }
}
