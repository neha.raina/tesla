<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Reservation;

/**
 * SearchReservation represents the model behind the search form about `backend\models\Reservation`.
 */
class SearchReservation extends Reservation
{
    /**
     * @inheritdoc
     */
    public $fromDate;
    public $toDate;
    public $subTitle;
    public function rules()
    {
        return [
            [['pkReservationID', 'fkAdID', 'fkCategoryID', 'confirmation', 'person'], 'integer'],
            [['userName', 'userEmail', 'phoneNumber', 'bookingDate', 'bookingTime', 'timestamp','subTitle','fromDate','toDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $query = Reservation::find()->joinWith(['fkAd', 'fkCategory']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        //echo '<pre>';print_r($this); die;
        // grid filtering conditions
        $query->andFilterWhere([
            'pkReservationID' => $this->pkReservationID,
            'fkAdID' => $this->fkAdID,
            'person' => $this->person,
            //'confirmation' => $this->confirmation,
            //'person' => $this->person,
        ]);

        $query->andFilterWhere(['like', 'userName', $this->userName])
            ->andFilterWhere(['like', 'ad.subTitle', $this->subTitle])
            ->andFilterWhere(['like', 'phoneNumber', $this->phoneNumber])
            ->andFilterWhere(['like', 'bookingDate', $this->bookingDate]);

            $query->andFilterWhere(['<=', 'bookingDate', $this->toDate]);    
            $query->andFilterWhere(['>=', 'bookingDate', $this->fromDate]);

        return $dataProvider;
    }
}
