$( function() {
    $( "#ad-startdate" ).datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd',
    minDate: '+0d',
    });    
    
    $( "#ad-enddate" ).datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd',
    minDate: '+0d',
    });

    $( "#datepickerFrom" ).datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd',
    });

    $( "#datepickerTo" ).datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd',
    });


    });

$(document).ready(function(){

    $('#submitAd').click(function(){

        var startDate = $('#ad-startdate').val();
        var endDate = $('#ad-enddate').val();
        if((startDate) && (endDate))
        {
            if((new Date(endDate)) > (new Date(startDate)) ){
            return true;
            }
            else
            {
                swal('End Date should be greater than start Date.');
                return false;
            }
        }
    });

    $('#changeSubmit').click(function(){

    var newPass = $('#newPassword').val();
    var conPass = $('#reNewPassword').val();

    if((newPass != '') && (newPass.length < 4))
    {
         swal("Password should be atleast 4 characters.");
         return false;
    }
    else if(newPass != conPass)
    {
        swal("New Password doesn't match with Confirm Password.");
         return false;   
    }
    else
    {
        return true;
    }

    });

    $('.deleteAdImage').click(function(){

        var adID = $(this).attr('attr');
        var imgType = $(this).attr('imgType');

       swal({
          title: "Are you sure?",
          text: "You want to delete this Ad",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes, delete it!",
          closeOnConfirm: false
        },
        function(){

            $.ajax({
            type     :'POST',
            cache    : false,
            url      : 'delete-ad',
            data     : {deleteAdID:adID,adImgType:imgType},
            success  : function(response) {
                var json_obj = $.parseJSON(response);
                if(json_obj == 'deleted')
                {
                    swal("Deleted!", "Your Ad image has been deleted.", "success");
                    location.reload();
                }
                else
                {
                    swal("There is some error coming. Try gain later.", "error");   
                }
                
            }
            }); 
        });

    });

    $('#deleteVideo').click(function(){

        var adID = $(this).attr('attr');

          swal({
          title: "Are you sure?",
          text: "You want to delete this video",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes, delete it!",
          closeOnConfirm: false
        },
        function(){

            $.ajax({
            type     :'POST',
            cache    : false,
            url      : 'delete-video',
            data     : {deleteAdID:adID},
            success  : function(response) {
                var json_obj = $.parseJSON(response);
                if(json_obj == 'deleted')
                {
                    $('.video_'+adID).remove();
                    $('#deleteVideo').remove();
                    swal("Deleted!", "Your video has been deleted.", "success");
                }
                else
                {
                    swal("There is some error coming. Try gain later.", "error");   
                }
                
            }
            }); 
        });

    });
    
    $('#reservationImage').click(function(){

        var adID = $(this).attr('attr');

          swal({
          title: "Are you sure?",
          text: "You want to delete this image",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes, delete it!",
          closeOnConfirm: false
        },
        function(){

            $.ajax({
            type     :'POST',
            cache    : false,
            url      : 'delete-reservation-image',
            data     : {deleteAdID:adID},
            success  : function(response) {
                var json_obj = $.parseJSON(response);
                if(json_obj == 'deleted')
                {
                    $('.reservationImg').remove();
                    $('#reservationImage').remove();
                    swal("Deleted!", "Your image has been deleted.", "success");
                }
                else
                {
                    swal("There is some error coming. Try gain later.", "error");   
                }
                
            }
            }); 
        });

    });
    
    $('#selectAdOrder').change(function(){

        var value = $(this).val();
        if(value)
        {
            $('.formSetOrder').submit();
        }

    });

    $('.mysort').sortable();

    $('#savePosition').click(function(){
        var columns = [];

        $('ul.mysort li').each(function(){
           columns.push($(this).attr('id'));                
        });

        $.ajax({
        type     :'POST',
        cache    : false,
        url      : 'set-position',
        data     : {positionArray:columns},
        success  : function(response) {
            if(response == '1')
            {
                swal('The order has been saved.');
            }
            
        }
        });

    });

    $('#saveAdPosition').click(function(){
        var columns = [];
        var catID = $(this).attr('catid');
        $('ul.mysort li').each(function(){
           columns.push($(this).attr('id'));                
        });
        
        $.ajax({
        type     :'POST',
        cache    : false,
        url      : 'set-position',
        data     : {positionArray:columns, positionCatId : catID},
        success  : function(response) {
            if(response == '1')
            {
                swal('The order has been saved.');
            }
            
        }
        });

    });

    $('#ad-fkcategoryid').change(function(){
        var catID = $(this).val();
        if(catID)
        {
            $.ajax({
            type     :'POST',
            cache    : false,
            url      : 'check-dinning',
            data     : {catID:catID},
            success  : function(response) {
                var json_obj = $.parseJSON(response);
                if(json_obj.categoryName)
                {
                    $('#dinningHidden').val('1');
                    $('.adMenuDIV').show();
                }
                else
                {
                    $('#dinningHidden').val('0');   
                    $('.adMenuDIV').hide();
                }
            }
            });
        }
    });


    $('#category-dinning').click(function(){
            
        var status;
        var catID = $('#categoryID').val();
        if($(this).is(":checked")){
            status = '1';         

        $.ajax({
        type     :'POST',
        cache    : false,
        url      : 'check-dinning',
        data     : {catID:catID},
        success  : function(response) {
            var json_obj = $.parseJSON(response);
            if(json_obj.categoryName)
            {
                swal('It seems that you have already assign dinning to "'+json_obj.categoryName+'" category. So, you cannot assign the same to more than one category.');
                $('#category-dinning').attr('checked', false);
            }
        }
        });

    }

    });



    $(".time-toggle").click (function()
    {
        $(".timer-box").toggle();
    });


   function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputFile").change(function () {
        readURL(this);
    });
	
    
});
/* ///////////////////////////////////////////////////////////////////// 
// Preloader 
/////////////////////////////////////////////////////////////////////*/
 $(window).load(function(){
    $('#preloader').fadeOut('slow',function(){$(this).remove();});
  });


  


