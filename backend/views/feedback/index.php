<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FeedbackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'All Feedbacks';
//$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </section>
    <section class="content">

<?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="col-md-12">
    <div class="box">
    <div class="box-body">

      <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'Name',
                'value' => function($dataProvider){ 
                    //echo '<pre>'; print_r($dataProvider); echo '</pre>'; die();
                        $url = Url::to(['feedback/view']);
                        $finalUrl = $url.'?id='.$dataProvider->pkFeedbackID;
                      return '<u><a href="'.$finalUrl.'">'.'<span class="adInfo">'.$dataProvider->name.'</span>'.'</a></u>';
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'Email',
                'value' => 'emailID',
                'format' => 'html'
            ],
            [
                'attribute' => 'Message',
                'value' => function($dataProvider){ 
                 return '<small>'.$dataProvider->message.'</small>';
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'Feedback Date/Time',
                'value' => function($dataProvider){
                      $timestamp = $dataProvider->timestamp;
                      $newDate = gmdate("j F Y, g:i a", $timestamp);
                      return '<span style="color:#00a65a">'.$newDate.'</span>';
                },
                'format' => 'html'
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{view}{delete}'],
        ],
    ]); ?>
      
    </div>
    </div>
    </div>


</section>


