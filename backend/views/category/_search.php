<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CategorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="col-md-12 searchCat">
<div class="box">
<div class="box-header">
<h3>Search</h3>
</div>

<div class="box-body">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
         <div class="col-lg-3 col-sm-4">
<?php echo $form->field($model, 'categoryName')->textInput()->input('text', ['placeholder' => "Category Name"])->label(false); ?>
         </div>
         <div class="col-lg-3 col-sm-4">
<?php echo $form->field($model, 'dinning')->dropDownList(['' => 'Select Dinning', '1' => 'Yes', '0' => 'No'])->label(false); ?>
         </div>
         <div class="col-lg-3 col-sm-4">
<?php echo $form->field($model, 'active')->dropDownList(['' => 'Select Active Status', '1' => 'Yes', '0' => 'No'])->label(false); ?>
         </div>

    <div class="col-lg-3 col-sm-4">
        <?= Html::submitButton('Search', ['class' => 'btn btn-info']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    </div>
</div>
</div>
</div>
