<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Category */

$this->title = 'View Category';
//$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </section>

    <section class="content">
    <div class="col-md-6">
    <?= Html::a('Update', ['update', 'id' => $model->pkCategoryID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->pkCategoryID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this category?',
                'method' => 'post',
            ],
        ]) ?>
        </div>

<?php
$pkCategoryID = (isset($model->pkCategoryID)) ? $model->pkCategoryID : '';
$categoryName = (isset($model->categoryName)) ? $model->categoryName : '';
$position = (isset($model->position)) ? $model->position : '';
$videoPath = (isset($model->videoPath)) ? $model->videoPath : '';
$timestamp = (isset($model->timestamp)) ? $model->timestamp : '';
?>
<div class="row">
    <div class="col-md-8 viewCat">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Category Information</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped catView">
                <tbody>
                <tr>
                  <th>Category Name</th>
                  <td><?php echo $categoryName;?></td>
                </tr>
                <tr>
                  <th>Dinning</th>
                  <td><?php if($model->dinning == '1'){?>
                    <i class="fa fa-check" aria-hidden="true"></i>
                   <?php }else{?>
                   <i class="fa fa-times" aria-hidden="true"></i>
                   <?php }?></td>
                </tr>
                <tr>
                  <th>Active</th>
                  <td><?php if($model->active == '1'){?>
                    <i class="fa fa-check" aria-hidden="true"></i>
                   <?php }else{?>
                   <i class="fa fa-times" aria-hidden="true"></i>
                   <?php }?></td>
                </tr>
                <tr>
                  <th>Positon Number</th>
                  <td>
                  <div class="position-info">
                     <b><?php echo $position;?></b>
                  </div>
                   </td>
                </tr>                
                <tr>
                  <th>Date & Time</th>
                  <td><?php 
                  echo gmdate("j F Y, g:i a", $timestamp);;?></td>
                </tr>
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
          </div>
          </div>

 </section>