<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Ad;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'All Categories';
//$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </section>
    

<section class="content">
<div class="col-md-6">

        <?= Html::a('Create Category', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Set Order', ['position'], ['class' => 'btn btn-info']) ?>
    </div>
<?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="col-md-12">
    <div class="box">
    <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'Category Name',
                'value' => function($dataProvider){ 
                    $url = Url::to(['category/view']);
                    $finalUrl = $url.'?id='.$dataProvider->pkCategoryID;
                        return '<u><a href="'.$finalUrl.'">'.$dataProvider->categoryName.'</a></u>';
                },
                'format' => 'html'
            ],          
            [
                'attribute' => 'Ads',
                'value' => function($dataProvider){ 
                    //echo '<pre>'; print_r($dataProvider); echo '</pre>'; die();
                    $numAd = Ad::find()->where(['fkCategoryID' => $dataProvider->pkCategoryID])->count();
                    $url = Url::to(['ad/index']);
                    $finalUrl = $url.'?AdSearch[fkCategoryID]='.$dataProvider->pkCategoryID;
                        return '<u><a href="'.$finalUrl.'"><span class="adInfo">'.$numAd.'</span></a></u>';
                },
                'format' => 'html'
                            
            ],
            [
                'attribute' => 'Order',
                'value' => function($dataProvider){
                        return '<span class="positionInfo">'.$dataProvider->position.'</span>';
                },
                'format' => 'html'
                            
            ],
            [
                'attribute' => 'Dinning',
                'value' => function($dataProvider){ 
                    if ($dataProvider->dinning == '1')
                        return '<i class="fa fa-check" aria-hidden="true"></i>';
                    else
                        return '<i class="fa fa-times" aria-hidden="true"></i>';
                },
                'format' => 'html'
                            
            ],
            [
                'attribute' => 'Active',
                'value' => function($dataProvider){ 
                    if ($dataProvider->active == '1')
                        return '<i class="fa fa-check" aria-hidden="true"></i>';
                    else
                        return '<i class="fa fa-times" aria-hidden="true"></i>';
                },
                'format' => 'html'
                            
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
    </div>
    </div>


</section>