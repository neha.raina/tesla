<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="col-md-10">
<div class="box box-info">

            <!-- form start -->
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Category Name</label>

                  <div class="col-sm-10">
                    <?= $form->field($model, 'categoryName')->textInput()->input('text', ['placeholder' => "Category Name", 'class' => 'form-control'])->label(false); ?>
                 </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Dinning</label>
                  <div class="col-sm-10">
                   <!-- <input type="checkbox" name="dinning" id="dinning" value="1">Dinning -->
                    <?php echo $form->field($model,'dinning')->checkBox()->label(false); ?>
                    <input type="hidden" id="categoryID" value="<?php if(isset($model->update)){ echo $model->pkCategoryID; }?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Status</label>
                  <div class="col-sm-10 checkbox">
                   <!-- <input type="checkbox" name="dinning" id="dinning" value="1">Dinning -->
                   <div class="col-lg-6">
                    <?= $form->field($model, 'active')->radio(['label' => 'Active', 'value' => 1, 'checked' => 'checked']) ?>
                    </div>
                    <div class="col-lg-6">
                    <?= $form->field($model, 'active')->radio(['label' => 'In Active', 'value' => 0, 'uncheck' => null]) ?>
                  </div>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                 <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
              </div>
              <!-- /.box-footer -->
            <?php ActiveForm::end(); ?>
          </div>
          </div>




