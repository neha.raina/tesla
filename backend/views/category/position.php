<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'All Categories';
//$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </section>
    
    <section class="content">
    <div class="col-md-12">

        <?= Html::a('Create Category', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('View Category', ['index'], ['class' => 'btn btn-info']) ?>
    </div>
    <div class="row">
    <div class="col-md-4 catsort">

    <div class="box">
    <div class="box-header">
              <h3 class="box-title">Arrange Categories</h3>
            </div>
    <ul class="mysort">
        <?php 
    foreach ($model as $result)
    { ?>
    <li class="sortable-item" id="<?php echo $result->pkCategoryID;?>"><?php echo $result->categoryName;?></li>
    <?php 
    }
    ?>    
    </ul>
       <button type="button" id="savePosition" class="btn btn-block btn-success btn-lg">Save</button>
    </div>
    </div>
    </div>
    </section>

    

    
    
    
    
