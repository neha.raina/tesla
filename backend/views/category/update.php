<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Category */

$this->title = 'Update Category';
//$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->pkCategoryID, 'url' => ['view', 'id' => $model->pkCategoryID]];
//$this->params['breadcrumbs'][] = 'Update';
?>
<section class="content-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </section>

<section class="content">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
 </section>
