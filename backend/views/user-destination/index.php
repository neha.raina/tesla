<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserDestinationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'All Destinations';
//$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </section>
    <section class="content">

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="col-md-12">
    <div class="box">
    <div class="box-body">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'Phone Number',
                'value' => function($dataProvider){ 
                    //echo '<pre>'; print_r($dataProvider); echo '</pre>'; die();
                        $url = Url::to(['user-destination/view']);
                        $finalUrl = $url.'?id='.$dataProvider->pkUserDestinationID;
                      return '<u><a href="'.$finalUrl.'">'.'<span class="adInfo">'.$dataProvider->phoneNumber.'</span>'.'</a></u>';
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'Latitude',
                'value' => 'latitude',
                'format' => 'html'
            ],
            [
                'attribute' => 'Longitude',
                'value' => 'longitude',
                'format' => 'html'
            ],
            [
                'attribute' => 'Date/Time',
                'value' => function($dataProvider){
                      $timestamp = $dataProvider->timestamp;
                      $newDate = gmdate("j F Y, g:i a", $timestamp);
                      return '<span style="color:#00a65a">'.$newDate.'</span>';
                },
                'format' => 'html'
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{view}{delete}'],
        ],
    ]); ?>

 </div>
    </div>
    </div>


</section>
