<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\UserDestination */

$this->title = 'View Destination';
// $this->params['breadcrumbs'][] = ['label' => 'User Destinations', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </section>

    <section class="content">
    <div class="col-md-6">
        <?= Html::a('Delete', ['delete', 'id' => $model->pkUserDestinationID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </div>

<?php
$phoneNumber = (isset($model->phoneNumber)) ? $model->phoneNumber : '';
$latitude = (isset($model->latitude)) ? $model->latitude : '';
$longitude = (isset($model->longitude)) ? $model->longitude : '';
$timestamp = (isset($model->timestamp)) ? $model->timestamp : '';
?>
    <div class="row">
    <div class="col-md-8 viewCat">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Destination Information</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped catView">
                <tbody>
                <tr>
                  <th>Phone Number</th>
                  <td><span class="adInfo"><?php echo $phoneNumber;?></span></td>
                </tr>
                <tr>
                  <th>Latitude</th>
                  <td><?php echo $latitude;?></td>
                </tr>
                <tr>
                  <th>Longitude</th>
                  <td><?php echo $longitude;?></td>
                </tr>
                <tr>
                  <th>Date & Time</th>
                  <td><?php 
                  echo gmdate("j F Y, g:i a", $timestamp);?></td>
                </tr>
              </tbody></table>
            </div>
          </div>
          </div>
</div>
