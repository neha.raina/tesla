<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\UserDestination */

$this->title = 'Update User Destination: ' . $model->pkUserDestinationID;
$this->params['breadcrumbs'][] = ['label' => 'User Destinations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pkUserDestinationID, 'url' => ['view', 'id' => $model->pkUserDestinationID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-destination-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
