<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Category;

/* @var $this yii\web\View */
/* @var $model backend\models\AdSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="col-md-12 searchCat">
<div class="box">
<div class="box-header">
<h3>Search</h3>
</div>

<div class="box-body">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
    <div class="col-lg-3 col-sm-4">
    <?php
    $items = ArrayHelper::map(Category::find()->orderBy(['position' => SORT_ASC])->all(), 'pkCategoryID', 'categoryName');
          
    echo $form->field($model, 'fkCategoryID')->dropdownList($items,
    ['prompt'=>'Select Category'])->label(false);
    ?>
         </div>
         <div class="col-lg-3 col-sm-4">
<?php echo $form->field($model, 'title')->textInput()->input('text', ['placeholder' => "Ad Title"])->label(false); ?>
         </div>
         <div class="col-lg-3 col-sm-4">
<?php echo $form->field($model, 'dinning')->dropDownList(['' => 'Select Dinning', '1' => 'Yes', '0' => 'No'])->label(false); ?>
         </div>
         <div class="col-lg-3 col-sm-4">
<?php echo $form->field($model, 'active')->dropDownList(['' => 'Select Active Status', '1' => 'Yes', '0' => 'No'])->label(false); ?>
         </div>

    <div class="col-lg-3 col-sm-4">
        <?= Html::submitButton('Search', ['class' => 'btn btn-info']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    </div>
</div>
</div>
</div>
