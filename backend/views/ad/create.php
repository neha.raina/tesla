<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Ad */

$this->title = 'Add Ad';
//$this->params['breadcrumbs'][] = ['label' => 'Ads', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </section>

<section class="content">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
 </section>
