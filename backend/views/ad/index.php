<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Category;
use backend\models\AdImage;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\AdSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'All Ads';
//$this->params['breadcrumbs'][] = $this->title;
?>

<section class="content-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </section>

<section class="content">
<div class="col-md-6">

        <?= Html::a('Create Ad', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Set Order', ['position'], ['class' => 'btn btn-info']) ?>
    </div>
<?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="col-md-12">
    <div class="box">
    <div class="box-body">


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'Ad Title',
                'value' => function($dataProvider){ 
                    $url = Url::to(['ad/view']);
                    $finalUrl = $url.'?id='.$dataProvider->pkAdID;
                        return '<u><a href="'.$finalUrl.'">'.$dataProvider->title.'</a></u>';
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'Category Name',
                'value' => function($dataProvider){ 
                        $url = Url::to(['category/view']);
                        $finalUrl = $url.'?id='.$dataProvider->fkCategory->pkCategoryID;
                      $catName = $dataProvider->fkCategory->categoryName;
                      return '<u><a href="'.$finalUrl.'"><span class="adInfo">'.$catName.'</span></a></u>';
                },
                'format' => 'html'
            ],
            [
                'header' => 'Order<br>(Cat wise)',
                'attribute' => 'Order',
                'value' => function($dataProvider){
                    //echo '<pre>'; print_r($dataProvider->categoryAdOrders[0]->position); echo '</pre>';
                        return '<span class="positionInfo">'.$dataProvider->categoryAdOrders[0]->position.'</span>';
                },
                'format' => 'html'
                            
            ],
            [
                'attribute' => 'Dinning',
                'value' => function($dataProvider){ 
                    if ($dataProvider->dinning == '1')
                        return '<i class="fa fa-check" aria-hidden="true"></i>';
                    else
                        return '<i class="fa fa-times" aria-hidden="true"></i>';
                },
                'format' => 'html'
                            
            ],
            [
                'attribute' => 'Active',
                'value' => function($dataProvider){ 
                    if ($dataProvider->active == '1')
                        return '<i class="fa fa-check" aria-hidden="true"></i>';
                    else
                        return '<i class="fa fa-times" aria-hidden="true"></i>';
                },
                'format' => 'html'
                            
            ],
            [
                'attribute' => '360 Video',
                'value' => function($dataProvider){ 
                    if ($dataProvider->videoPath)
                        return '<i class="fa fa-check" aria-hidden="true"></i>';
                    else
                        return '<i class="fa fa-times" aria-hidden="true"></i>';
                },
                'format' => 'html'
                            
            ],
            [
                'attribute' => 'Splash Image',
                'value' => function($dataProvider){ 
                    $splashImage = AdImage::find()->where(['fkAdID' => $dataProvider->pkAdID, 'imageType' => 'splashImage'])->one();
                        if(isset($splashImage->imagePath))
                        return '<i class="fa fa-check" aria-hidden="true"></i>';
                        else
                        return '<i class="fa fa-times" aria-hidden="true"></i>';
                },
                'format' => 'html'
                            
            ],
            [
                'attribute' => 'Main Image',
                'value' => function($dataProvider){ 
                    $splashImage = AdImage::find()->where(['fkAdID' => $dataProvider->pkAdID, 'imageType' => 'mainImage'])->one();
                        if(isset($splashImage->imagePath))
                        return '<i class="fa fa-check" aria-hidden="true"></i>';
                        else
                        return '<i class="fa fa-times" aria-hidden="true"></i>';
                },
                'format' => 'html'
                            
            ],
            [
                'attribute' => 'Left Image',
                'value' => function($dataProvider){ 
                    $splashImage = AdImage::find()->where(['fkAdID' => $dataProvider->pkAdID, 'imageType' => 'leftImage'])->one();
                        if(isset($splashImage->imagePath))
                        return '<i class="fa fa-check" aria-hidden="true"></i>';
                        else
                        return '<i class="fa fa-times" aria-hidden="true"></i>';
                },
                'format' => 'html'
                            
            ],
            [
                'attribute' => 'Right Image',
                'value' => function($dataProvider){ 
                    $splashImage = AdImage::find()->where(['fkAdID' => $dataProvider->pkAdID, 'imageType' => 'rightImage'])->one();
                        if(isset($splashImage->imagePath))
                        return '<i class="fa fa-check" aria-hidden="true"></i>';
                        else
                        return '<i class="fa fa-times" aria-hidden="true"></i>';
                },
                'format' => 'html'
                            
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    </div>
    </div>
    </div>


</section>
