<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Ad */

$this->title = 'Update Ad';
// $this->params['breadcrumbs'][] = ['label' => 'Ads', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->pkAdID]];
// $this->params['breadcrumbs'][] = 'Update';
?>
<section class="content-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </section>

<section class="content">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
 </section>
