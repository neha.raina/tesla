<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Category;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'All Ads';
//$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </section>
    
    <section class="content">
    <div class="col-md-12">

        <?= Html::a('Create Ad', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('View Ads', ['index'], ['class' => 'btn btn-info']) ?>
    </div>

    <div class="timeline-item">
        <h3 class="timeline-header ">Order wise categories</h3>
    </div>
    <?php

    $allCats = Category::find()->orderBy(['position' => SORT_ASC])->all();
    ?>
    <div class="row">

        <div class="col-md-2">
        <div class="form-group field-categorysearch-dinning has-success">

       <?php $form = ActiveForm::begin(['options' => ['class' => 'formSetOrder']]) ?>
        <select id="selectAdOrder" class="form-control" name="selectAdOrder">
        <option value="">Select Category</option>
        <?php
        foreach($allCats as $allCategories)
        {
        ?>
        <option value="<?php echo $allCategories->pkCategoryID;?>" <?php if((isset($model['formCategoryID'])) && ($allCategories->pkCategoryID == trim($model['formCategoryID']))){ echo 'selected';}?>><?php echo $allCategories->categoryName?></option>
        <?php 
        }
        ?>
        </select>
        <?php ActiveForm::end(); ?>
        <div class="help-block"></div>
        </div>         
        </div>
<div class="clearfix"></div>
    <div class="col-md-4 catsort">

    <div class="box">
    <div class="box-header">
              <h3 class="box-title">Arrange Ads</h3>
            </div>
    <ul class="mysort">
        <?php 
        //echo '<pre>'; print_r($model); echo '</pre>';
        if(isset($model[0])){
        $count = count($model) - 1;
        for($i=0; $i< $count; $i++)
        {
        ?>
        <li class="sortable-item" id="<?php echo $model[$i]->pkAdID;?>"><?php echo $model[$i]->title;?></li>
        <?php
        }
        }
        else
        {
        ?>
        <li class="sortable-item">No Result Found</li>
        <?php
        }
       ?>  
    </ul>
    <?php 
    if(isset($model[0])){
    ?>
       <button type="button" catid="<?php echo $model['formCategoryID'];?>" id="saveAdPosition" class="btn btn-block btn-success btn-lg">Save</button>
    <?php }?>
    </div>
    </div>
    </div>
    </section>

    

    
    
    
    
