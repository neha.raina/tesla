<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use backend\models\Category;
use backend\models\AdImage;


/* @var $this yii\web\View */
/* @var $model backend\models\Ad */
/* @var $form yii\widgets\ActiveForm */
 // echo '<pre>'; print_r($model); echo '</pre>'; 
?>
<?php
  $adStatusValue = $model->adStatus;
?>
<?php if($adStatusValue == 'create'){
?>
<style>
.adMenuDIV{
  display:none;
}
</style>
<?php
  }
  ?>
<div class="col-md-10">
<div class="box box-info">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
    <div class="box-body">
        
        <?php
        $items = ArrayHelper::map(Category::find()->all(), 'pkCategoryID', 'categoryName');
        ?>
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Select Category</label>

            <div class="col-sm-6"> 
            <?= $form->field($model, 'fkCategoryID')->dropdownList($items,['prompt'=>'Select Category'])->label(false); ?>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Status</label>
                  <div class="col-sm-10 checkbox">
                   <!-- <input type="checkbox" name="dinning" id="dinning" value="1">Dinning -->
                   <div class="col-lg-6">
                    <?= $form->field($model, 'active')->radio(['label' => 'Active', 'value' => 1, 'checked' => 'checked']) ?>
                    </div>
                    <div class="col-lg-6">
                    <?= $form->field($model, 'active')->radio(['label' => 'In Active', 'value' => 0, 'uncheck' => null]) ?>
                  </div>
                  </div>
                </div>
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Ad Title</label>

            <div class="col-sm-10"> 
            <?= $form->field($model, 'title')->textInput()->input('text', ['placeholder' => "Ad Title", 'class' => 'form-control'])->label(false); ?>
            </div>
        </div>
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Sub-Title</label>

            <div class="col-sm-10"> 
            <?= $form->field($model, 'subTitle')->textInput()->input('text', ['placeholder' => "Sub-Title", 'class' => 'form-control'])->label(false); ?>
            </div>
        </div>
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Header</label>

            <div class="col-sm-10"> 
            <?= $form->field($model, 'header')->textInput()->input('text', ['placeholder' => "Header", 'class' => 'form-control'])->label(false); ?>
            </div>
        </div>
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Description</label>

            <div class="col-sm-10"> 
            <?= $form->field($model, 'description')->textarea(['rows' => '4','placeholder' => "Description", 'class' => 'form-control'])->label(false); ?>
            </div>
        </div>
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Address</label>

            <div class="col-sm-4"> 
            <?= $form->field($model, 'address')->textInput()->input('text', ['placeholder' => "Address", 'class' => 'form-control'])->label(false); ?>
            </div>
       
          <label for="inputEmail3" class="col-sm-2 control-label">Street Name</label>

            <div class="col-sm-4"> 
            <?= $form->field($model, 'streetName')->textInput()->input('text', ['placeholder' => "Street Name", 'class' => 'form-control'])->label(false); ?>
            </div>
        </div>

        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">City Name</label>

            <div class="col-sm-4"> 
            <?= $form->field($model, 'cityName')->textInput()->input('text', ['placeholder' => "City Name", 'class' => 'form-control'])->label(false); ?>
            </div>
       
          <label for="inputEmail3" class="col-sm-2 control-label">Country Name</label>

            <div class="col-sm-4"> 
            <?= $form->field($model, 'countryName')->textInput()->input('text', ['placeholder' => "Country Name", 'class' => 'form-control'])->label(false); ?>
            </div>
        </div>
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Latitude</label>

            <div class="col-sm-4"> 
            <?= $form->field($model, 'latitude')->textInput()->input('text', ['placeholder' => "Latitude", 'class' => 'form-control'])->label(false); ?>
            </div>
       
          <label for="inputEmail3" class="col-sm-2 control-label">Longitude</label>

            <div class="col-sm-4"> 
            <?= $form->field($model, 'longitude')->textInput()->input('text', ['placeholder' => "Longitude", 'class' => 'form-control'])->label(false); ?>
            </div>
        </div>
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Contact Number</label>

            <div class="col-sm-10"> 
            <?= $form->field($model, 'contactNumber')->textInput()->input('text', ['placeholder' => "Contact Number", 'class' => 'form-control'])->label(false); ?>
            </div>
        </div>
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Start Date</label>

            <div class="col-sm-4"> 
            <?= $form->field($model, 'startDate')->textInput()->input('text', ['placeholder' => "Start Date", 'class' => 'form-control'])->label(false); ?>
            </div>
       
          <label for="inputEmail3" class="col-sm-2 control-label">End Date</label>

            <div class="col-sm-4"> 
            <?= $form->field($model, 'endDate')->textInput()->input('text', ['placeholder' => "End Date", 'class' => 'form-control'])->label(false); ?>
            </div>
        </div>
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Start Time</label>

            <div class="col-sm-4"> 
            <?= $form->field($model, 'startDateTime')->textInput()->input('text', ['placeholder' => "Start Time (HH:MM:SS)(24 hrs)", 'class' => 'form-control'])->label(false); ?>
            </div>
       
          <label for="inputEmail3" class="col-sm-2 control-label">End Time</label>

            <div class="col-sm-4"> 
            <?= $form->field($model, 'endDateTime')->textInput()->input('text', ['placeholder' => "End Time (HH:MM:SS)(24 hrs)", 'class' => 'form-control'])->label(false); ?>
            </div>    
        </div>
        <?php
        if($adStatusValue == 'update')
        {
          $splashImage = AdImage::find()->where(['fkAdID' => $model->pkAdID , 'imageType' => 'splashImage'])->one();
          $mainImage = AdImage::find()->where(['fkAdID' => $model->pkAdID , 'imageType' => 'mainImage'])->one();
          $leftImage = AdImage::find()->where(['fkAdID' => $model->pkAdID , 'imageType' => 'leftImage'])->one();
          $rightImage = AdImage::find()->where(['fkAdID' => $model->pkAdID , 'imageType' => 'rightImage'])->one();
        
          $myBaseUrl = Url::base(true);
          if($splashImage){ $splashImageUrl = $myBaseUrl.'/ads/'.$splashImage->imagePath; }else{ $splashImageUrl = $myBaseUrl.'/images/no-image-icon.jpg';}
          if($mainImage){ $mainImageUrl = $myBaseUrl.'/ads/'.$mainImage->imagePath; }else{ $mainImageUrl = $myBaseUrl.'/images/no-image-icon.jpg';}
          if($leftImage){ $leftImageUrl = $myBaseUrl.'/ads/'.$leftImage->imagePath; }else{ $leftImageUrl = $myBaseUrl.'/images/no-image-icon.jpg';}
          if($rightImage){ $rightImageUrl = $myBaseUrl.'/ads/'.$rightImage->imagePath; }else{ $rightImageUrl = $myBaseUrl.'/images/no-image-icon.jpg';}
        
          if($model->reservationImage)
          {
            $reservationImageUrl = $myBaseUrl.'/ads/reservation/'.$model->reservationImage;
          }
          else
          {
            $reservationImageUrl = $myBaseUrl.'/images/no-image-icon.jpg';
          }
        }
        ?>
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Splash Image</label>

            <div class="col-sm-4 col-xs-12">
            <?php if($adStatusValue == 'update')
            { ?>
            <img class="img-responsive adImageresp" src="<?php echo $splashImageUrl;?>" alt="Photo">
            <?php if(isset($splashImage->imagePath)){ ?>
            <button style="margin-top:10px; width:100px" type="button" attr="<?php echo $model->pkAdID;?>" imgtype="splashImage" class="btn btn-block btn-primary deleteAdImage">Delete</button>
            <?php }?>
            <br>
            <?php }?>
            <input type="file" name="splashImage">
            </div>
       
          <label for="inputEmail3" class="col-sm-2 control-label">Main Image</label>

            <div class="col-sm-4 col-xs-12">
            <?php if($adStatusValue == 'update')
            { ?>
            <img class="img-responsive adImageresp" src="<?php echo $mainImageUrl;?>" alt="Photo">
            <?php if(isset($mainImage->imagePath)){ ?>
            <button style="margin-top:10px; width:100px" type="button" attr="<?php echo $model->pkAdID;?>" imgtype="mainImage" class="btn btn-block btn-primary deleteAdImage">Delete</button>
            <?php }?>
            <br>
            <?php }?>
            <input type="file" name="mainImage">
            </div>    
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Secondary left image</label>

            <div class="col-sm-4">
            <?php if($adStatusValue == 'update')
            { ?>
            <img class="img-responsive adImageresp" src="<?php echo $leftImageUrl;?>" alt="Photo">
            <?php if(isset($leftImage->imagePath)){ ?>
            <button style="margin-top:10px; width:100px" type="button" attr="<?php echo $model->pkAdID;?>" imgtype="leftImage" class="btn btn-block btn-primary deleteAdImage">Delete</button>
            <?php }?>
            <br>
            <?php }?>
            <input type="file" name="leftImage">
            </div>
       
          <label for="inputEmail3" class="col-sm-2 control-label">Secondary right image</label>

            <div class="col-sm-4">
            <?php if($adStatusValue == 'update')
            { ?>
            <img class="img-responsive adImageresp" src="<?php echo $rightImageUrl;?>" alt="Photo">
            <?php if(isset($rightImage->imagePath)){ ?> 
            <button style="margin-top:10px; width:100px" type="button" attr="<?php echo $model->pkAdID;?>" imgtype="rightImage" class="btn btn-block btn-primary deleteAdImage" >Delete</button>
            <?php }?>
            <br>
            <?php }?>
            <input type="file" name="rightImage">
            </div>    
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
          <label for="exampleInputFile" class="col-sm-2 control-label">360&deg; Video</label>
          <div class="col-sm-10">
          <?php if($adStatusValue == 'update'){
                if($model->videoPath)
                {
                    $myBaseUrl = Url::base(true);
                    $videoUrl = $myBaseUrl.'/ads/videos/'.$model->videoPath;
                ?>
                      <video width="320" height="240" controls class="video_<?php echo $model->pkAdID;?>">
                      <source src="<?php echo $videoUrl;;?>" type="video/mp4">
                      </video>
                      <div class="clearfix"></div>
                      <div class="col-sm-2">
                      <button style="margin-bottom:10px;" type="button" attr="<?php echo $model->pkAdID;?>" class="btn btn-block btn-primary" id="deleteVideo">Delete</button>
                      </div>
                      <div class="clearfix"></div>
            <?php
            }else{ 
              $videoNotFound = $myBaseUrl.'/images/no-video.jpg';
              ?>
            <img class="img-responsive adImageresp reservationImg" src="<?php echo $videoNotFound;?>" alt="Photo">
            <?php
            }
            }
            ?>
            <input type="file" name="adVideo">
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Reservation Message</label>

            <div class="col-sm-4"> 
            <?= $form->field($model, 'reservationMessage')->textInput()->input('text', ['placeholder' => "Reservation Message", 'class' => 'form-control  '])->label(false); ?>
            </div>
       
          <label for="inputEmail3" class="col-sm-2 control-label">Reservation Image</label>

            <div class="col-sm-4">
            <?php if($adStatusValue == 'update')
            { ?>
            <img class="img-responsive adImageresp reservationImg" src="<?php echo $reservationImageUrl;?>" alt="Photo">
            <?php if(isset($model->reservationImage)){ ?> 
            <button style="margin-top:10px; width:100px" type="button" attr="<?php echo $model->pkAdID;?>" id="reservationImage" class="btn btn-block btn-primary" >Delete</button>
            <?php }?>
            <br>
            <?php }?>
            <input type="file" name="reservationImage">
            </div>    
        </div>
        <div class="clearfix"></div>
        <?php
               if($model->dinning == '0')
               {
                    $dinningValue = '0';
               }
               elseif($model->dinning == '1')
               {
                    $dinningValue = '1';
               }
               else
               {
                    $dinningValue = '0';
                    $dinningValue = '1';
               }

        ?>
        <?php if($adStatusValue == 'update'){
          if($dinningValue == '1')
          { ?>
            <style>.adMenuDIV{display:block;}</style>
          <?php
          }else{
          ?>
        <style>.adMenuDIV{display:none;}</style>
        <?php
        }
        }
        ?>
        <?php echo $form->field($model, 'dinning')->hiddenInput(['value' => $dinningValue,'id' => 'dinningHidden'])->label(false);?>
        <div class="clearfix"></div>
        <div class="form-group adMenuDIV">
        <div class="col-sm-12">
        <?php if($adStatusValue == 'create'){ ?>
        <span class="text-green">It seems that you have choose the dinning category. So, upload the Menu and write the Menu header name here.</span>
        <?php }?>
        </div>
          <label for="inputEmail3" class="col-sm-2 control-label">Menu Header</label>

            <div class="col-sm-4">
            <?php
             $menuHeader = (isset($model->adMenus[0]->menuHeader)) ? $model->adMenus[0]->menuHeader : '';
             $model->menuHeader = $menuHeader;
            ?>

            <?= $form->field($model, 'menuHeader')->textInput()->input('text', ['placeholder' => "Menu Header", 'class' => 'form-control'])->label(false); ?>
            </div>
       
            <label for="inputEmail3" class="col-sm-2 control-label">Menu PDF</label>
            <div class="col-sm-4">
            <?php if($adStatusValue == 'update')
            { 

              $menuPdfPath = (isset($model->adMenus[0]->menuPdfPath)) ? $model->adMenus[0]->menuPdfPath : '';
              $myBaseUrl = Url::base(true);
              if($menuPdfPath){
                $menuPdfPathUrl = $myBaseUrl.'/pdf/'.$menuPdfPath;  ?>

              <object data="<?php echo $menuPdfPathUrl;?>" type="application/pdf" width="100%" height="100%">
              </object>

              <?php
              }else{
                $menuPdfPathUrl = $myBaseUrl.'/images/nofile.png';
              ?>  
              <img class="img-responsive adImageresp" src="<?php echo $menuPdfPathUrl;?>" alt="Photo">
            <?php }
            }
            ?>
            <input type="file" name="menuPDF">
            </div>    
        </div>
        <div class="clearfix"></div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'id' => 'submitAd']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
