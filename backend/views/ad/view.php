<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use backend\models\AdImage;

/* @var $this yii\web\View */
/* @var $model backend\models\Ad */

$this->title = 'View Ad';
//$this->params['breadcrumbs'][] = ['label' => 'Ads', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </section>

    <section class="content">
    <div class="col-md-6">
    <?= Html::a('Update', ['update', 'id' => $model->pkAdID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->pkAdID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this Ad?',
                'method' => 'post',
            ],
        ]) ?>
    </div>
<?php
//echo '<pre>'; print_r($model); echo '</pre>'; 
$pkAdID = (isset($model->pkAdID)) ? $model->pkAdID : '';
$categoryName = (isset($model->fkCategory->categoryName)) ? $model->fkCategory->categoryName : '';
$position = (isset($model->position)) ? $model->position : '';
$title = (isset($model->title)) ? $model->title : '';
$subTitle = (isset($model->subTitle)) ? $model->subTitle : '';
$header = (isset($model->header)) ? $model->header : '';
$description = (isset($model->description)) ? $model->description : '';
$address = (isset($model->address)) ? $model->address : '';
$streetName = (isset($model->streetName)) ? $model->streetName : '';
$cityName = (isset($model->cityName)) ? $model->cityName : '';
$countryName = (isset($model->countryName)) ? $model->countryName : '';
$latitude = (isset($model->latitude)) ? $model->latitude : '';
$longitude = (isset($model->longitude)) ? $model->longitude : ''; 
$finalAddress = $address.' '.$streetName.' '.$cityName.' '.$countryName;
$contactNumber = (isset($model->contactNumber)) ? $model->contactNumber : '';
$startDate = (isset($model->startDate)) ? $model->startDate : '';
$endDate = (isset($model->endDate)) ? $model->endDate : '';
$videoPath = (isset($model->videoPath)) ? $model->videoPath : '';
$reservationImage = (isset($model->reservationImage)) ? $model->reservationImage : '';
$reservationMessage = (isset($model->reservationMessage)) ? $model->reservationMessage : '';

if($startDate){ $newStartDate = date("j F Y", strtotime($startDate));}else{$newStartDate = NULL;}
if($endDate){ $newEndDate = date("j F Y", strtotime($endDate));}else{$newEndDate = NULL;}
$startDateTime = (isset($model->startDateTime)) ? $model->startDateTime : '';
$endDateTime = (isset($model->endDateTime)) ? $model->endDateTime : '';
$timestamp = (isset($model->timestamp)) ? $model->timestamp : '';

$menuHeader = (isset($model->adMenus[0]->menuHeader)) ? $model->adMenus[0]->menuHeader : '';
$menuPdfPath = (isset($model->adMenus[0]->menuPdfPath)) ? $model->adMenus[0]->menuPdfPath : '';

  $splashImage = AdImage::find()->where(['fkAdID' => $model->pkAdID , 'imageType' => 'splashImage'])->one();
  $mainImage = AdImage::find()->where(['fkAdID' => $model->pkAdID , 'imageType' => 'mainImage'])->one();
  $leftImage = AdImage::find()->where(['fkAdID' => $model->pkAdID , 'imageType' => 'leftImage'])->one();
  $rightImage = AdImage::find()->where(['fkAdID' => $model->pkAdID , 'imageType' => 'rightImage'])->one();

  $myBaseUrl = Url::base(true);
  if($splashImage){ $splashImageUrl = $myBaseUrl.'/ads/'.$splashImage->imagePath; }else{ $splashImageUrl = $myBaseUrl.'/images/no-image-icon.jpg';}
  if($mainImage){ $mainImageUrl = $myBaseUrl.'/ads/'.$mainImage->imagePath; }else{ $mainImageUrl = $myBaseUrl.'/images/no-image-icon.jpg';}
  if($leftImage){ $leftImageUrl = $myBaseUrl.'/ads/'.$leftImage->imagePath; }else{ $leftImageUrl = $myBaseUrl.'/images/no-image-icon.jpg';}
  if($rightImage){ $rightImageUrl = $myBaseUrl.'/ads/'.$rightImage->imagePath; }else{ $rightImageUrl = $myBaseUrl.'/images/no-image-icon.jpg';}
  if($reservationImage){ $reservationImageUrl =  $myBaseUrl.'/ads/reservation/'.$reservationImage;}else{ $reservationImageUrl = $myBaseUrl.'/images/no-image-icon.jpg'; }
?>
        <div class="row">
        <div class="col-md-8 viewCat">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Ad Information</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped catView">
                <tbody>
                <tr>
                  <th>Title</th>
                  <td><?php echo $title;?></td>
                </tr>
                <tr>
                  <th>Sub Title</th>
                  <td><?php echo $subTitle;?></td>
                </tr>
                <tr>
                  <th>Category Name</th>
                  <td><?php echo $categoryName;?></td>
                </tr>
                <tr>
                  <th>Dinning</th>
                  <td><?php if($model->dinning == '1'){?>
                    <i class="fa fa-check" aria-hidden="true"></i>
                   <?php }else{?>
                   <i class="fa fa-times" aria-hidden="true"></i>
                   <?php }?></td>
                </tr>
                <tr>
                  <th>Active</th>
                  <td><?php if($model->active == '1'){?>
                    <i class="fa fa-check" aria-hidden="true"></i>
                   <?php }else{?>
                   <i class="fa fa-times" aria-hidden="true"></i>
                   <?php }?></td>
                </tr>
                <tr>
                  <th>Positon Number</th>
                  <td>
                  <div class="position-info">
                     <b><?php echo $position;?></b>
                  </div>
                   </td>
                </tr>
                <tr>
                  <th>Header</th>
                  <td><?php echo $header;?></td>
                </tr>
                <tr>
                  <th>Description</th>
                  <td><?php echo $description;?></td>
                </tr>
                <tr>
                  <th>Address</th>
                  <td><?php echo $finalAddress;?></td>
                </tr>
                <tr>
                  <th>Contact Number</th>
                  <td><?php echo $contactNumber;?></td>
                </tr>
                <tr>
                  <th>Latitude</th>
                  <td><?php echo $latitude;?></td>
                </tr>
                <tr>
                  <th>Longitude</th>
                  <td><?php echo $longitude;?></td>
                </tr>
                <tr>
                  <th>Start & End Date-Time</th>
                  <td><?php echo $newStartDate.' ('.$startDateTime.') - '.$newEndDate.' ('.$endDateTime.')';?></td>
                </tr>
                <tr>
                  <th class="col-sm-2">Splash Image</th>
                  <td class="col-sm-4"><img class="img-responsive adImageresp" src="<?php echo $splashImageUrl;?>"></td>
                </tr>
                <tr>
                  <th>Main Image</th>
                  <td><img class="img-responsive adImageresp" src="<?php echo $mainImageUrl;?>"></td>
                </tr>
                <tr>
                  <th>Left Image</th>
                  <td><img class="img-responsive adImageresp" src="<?php echo $leftImageUrl;?>"></td>
                </tr>
                <tr>
                  <th>Right Image</th>
                  <td><img class="img-responsive adImageresp" src="<?php echo $rightImageUrl;?>"></td>
                </tr>
                <tr>
                  <th>360&deg; Video</th>
                  <td>
                  <?php 
                        if($videoPath)
                        {
                            $myBaseUrl = Url::base(true);
                            $videoUrl = $myBaseUrl.'/ads/videos/'.$model->videoPath;
                  ?>
                        <video width="320" height="240" controls>
                        <source src="<?php echo $videoUrl;;?>" type="video/mp4">
                        </video>
                    <?php }
                    else
                    { 
                      $videoUrl = $myBaseUrl.'/images/no-video.jpg';
                      ?>
                    <img class="img-responsive adImageresp" src="<?php echo $videoUrl;?>">
                    <?php 
                    }
                    ?>
                  </td>
                </tr>
                <tr>
                  <th>Reservation Message</th>
                  <td><?php echo $reservationMessage;?></td>
                </tr>
                <tr>
                  <th>Reservation Image</th>
                  <td><img class="img-responsive adImageresp" src="<?php echo $reservationImageUrl;?>"></td>
                </tr>
                <?php if($model->dinning == '1'){?>
                <tr>
                  <th>Menu Header</th>
                  <td><?php echo $menuHeader;?></td>
                </tr>
                <tr>
                  <th>Menu PDF</th>
                  <td>
                    <?php 
                    $myBaseUrl = Url::base(true);
                    if($menuPdfPath){
                      $menuPdfPathUrl = $myBaseUrl.'/pdf/'.$menuPdfPath;  ?>

                    <object data="<?php echo $menuPdfPathUrl;?>" type="application/pdf" width="60%" height="100%">
                    </object>

                    <?php
                    }else{
                      $menuPdfPathUrl = $myBaseUrl.'/images/nofile.png';
                    ?>  
                    <img class="img-responsive adImageresp" src="<?php echo $menuPdfPathUrl;?>" alt="Photo">
                  <?php }
                  ?>
                  </td>
                </tr>
                <?php }?>
                <tr>
                  <th>Date & Time</th>
                  <td><?php 
                  echo gmdate("j F Y, g:i a", $timestamp);
                  ?>
                  </td>
                </tr>
              </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          </div>
          </div>

 </section>
</div>
