 <?php
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Url;
    use backend\models\Category;
    use backend\models\Ad;


    $this->title = 'Dashboard';
?>
<section class="content-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </section>

    <section class="content">
<?php 
$categoryCount = Category::find()->count();
$dinningCategory = Category::find()->select(['categoryName'])->where(['dinning' => '1'])->one();
if($dinningCategory){ $dinningCatName = $dinningCategory->categoryName; }else{ $dinningCatName = 'No Dinning added';}
$adCount = Ad::find()->count();
$dinningAdCount = Ad::find()->where(['dinning' => '1'])->count();

?>
    	<div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-tags"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Categories</span>
              <span class="info-box-number"><?php echo $categoryCount?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-cutlery"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Dinning Category</span>
              <span class="info-box-number"><?php echo $dinningCatName?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-picture-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Ads</span>
              <span class="info-box-number"><?php echo $adCount?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-cutlery"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Dinning Ads</span>
              <span class="info-box-number"><?php echo $dinningAdCount?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="row">

        <div class="col-md-6">

        <div class="box box-primary">
        	<div class="box-header with-border">
              <h3 class="box-title">Ads Vs Dinning Ads</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body" id="dinningChartContainer"></div>
        </div>
        </div>

        <div class="col-md-6">
        <div class="box box-primary">

        	<div class="box-header with-border">
              <h3 class="box-title">Total Number of bookings</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body" id="chartContainer"></div>
        </div>
        </div>

        </div>

    </section>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script>
    $(function () {

    	Highcharts.chart('dinningChartContainer', {
    title: {
        text: 'Ads Vs Dinning Ads',
        x: -20 //center
    },
    credits: {
 	         enabled: false
             },
    xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    },
    yAxis: {
        title: {
            text: 'Numbers'
        },
        plotLines: [{
            value: 0,
            width: 1,
            color: '#808080'
        }]
    },
    tooltip: {
        valueSuffix: ''
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle',
        borderWidth: 0
    },
    series: [{
        name: 'Normal Ads',
        data: [3, 4, 5, 8, 11, 15, 17, 16, 14, 10, 6, 4]
    },
    {
        name: 'Dinning Ads',
        data: [1, 2, 1, 4, 5, 2, 8, 5, 10, 2, 5, 4]
    }]
});


    Highcharts.chart('chartContainer', {
    title: {
        text: 'Monthly Dinning Bookings',
        x: -20 //center
    },
    credits: {
 	         enabled: false
             },
    xAxis: {
        categories: ['February 1', 'February 5', 'February 6', 'February 15', 'February 16', 'February 17',
            'February 20', 'February 22', 'February 24', 'February 25', 'February 28']
    },
    yAxis: {
        title: {
            text: 'Numbers'
        },
        plotLines: [{
            value: 0,
            width: 1,
            color: '#808080'
        }]
    },
    tooltip: {
        valueSuffix: ''
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle',
        borderWidth: 0
    },
    series: [{
        name: 'Bookings',
        data: [7, 6, 9, 14, 18, 21, 25, 26, 23, 18, 13]
    }]
});
});
    </script>