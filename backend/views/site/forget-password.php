<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
?>
<div class="login-box">
<div class="login-logo">
    <a href="#"><b>Cabitall</b></a>
  </div>
  <?php 
            
            if(isset($message)){ ?>
            <div class="alert alert-success">
  <strong><?php echo $message;?></strong>
</div>
            <?php
            }else{
              ?>
    <div class="signin_box">
        <div class="login-box-body">
            <p class="login-box-msg">Forgot your password?</p>
            <?php $form = ActiveForm::begin(['action' => '', 'options' => ['class' => 'form-signin']]); ?>   

            <label for="inputEmail" class="sr-only">Email address</label>
            <?php
            echo $form->field($model, 'adminEmail', [
                'inputOptions' => ['autofocus' => '', 'class' => 'form-control', 'id' => 'inputEmail']
            ])->textInput()->input('email', ['placeholder' => "Email"])->label(false);
            ?>
            <!--<label for="test1">Remember Me</label>-->       
            <br>
            <div class="row">
            <div class="col-xs-4">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-lg btn-primary btn-block', 'name' => 'login-button']) ?>
            </div>
            </div>
            <?php ActiveForm::end(); ?>
            
        </div>

    </div>
    <?php }?>
</div>

