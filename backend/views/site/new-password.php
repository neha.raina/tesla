<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
?>
<div class="login-box">
<div class="login-logo">
    <a href="#"><b>Cabitall</b></a>
  </div>
  <?php 
            if((isset($data['status'])) && ($data['status'] == 'Changed'))
            {
            ?>
             <div class="alert alert-success">
            <strong>Your password has been successfully changed.</strong>
            </div>    
            <?php
            }
            elseif((isset($data['status'])) && ($data['status'] == 'Error'))
            {
                ?>
            <div class="alert alert-danger">
            <strong>There is some error coming while resetting your password.</strong>
            </div>    
                <?php
            }
            else
            {
                $adminID = (isset($data['adminID'])) ? $data['adminID'] : NULL;
                if((isset($data['message'])) && ($data['message'] != 'active')){ ?>
                <div class="alert alert-danger">
                <strong><?php echo $data['message'];?></strong>
                </div>
                <?php
                }else{
               ?>
    <div class="signin_box">
        <div class="login-box-body">
            <p class="login-box-msg">Enter your new password</p>
            <?php $form = ActiveForm::begin(['action' => 'reset-password', 'options' => ['class' => 'form-signin']]); ?>   

            <label for="inputEmail" class="sr-only">Password</label>
            <?php
            echo $form->field($model, 'adminPassword', [
                'inputOptions' => ['autofocus' => '', 'class' => 'form-control']
            ])->textInput()->input('Password', ['placeholder' => "Password"])->label(false);
            ?>
            <?php
            echo $form->field($model, 'adminID')->hiddenInput(['value' => $adminID])->label(false);
            ?>

            <!--<label for="test1">Remember Me</label>-->       
            <br>
            <div class="row">
            <div class="col-xs-4">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-lg btn-primary btn-block', 'name' => 'login-button']) ?>
            </div>
            </div>
            <?php ActiveForm::end(); ?>
            
        </div>

    </div>
    <?php }
          }
    ?>
            
</div>

