<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
?>
<div class="login-box">
<div class="login-logo">
    <a href="#"><b>Cabitall</b></a>
  </div>
    <div class="signin_box">
        <div class="login-box-body">
            <p class="login-box-msg">Sign in to your account</p>


            <?php $form = ActiveForm::begin(['action' => '', 'options' => ['class' => 'form-signin']]); ?>   

            <label for="inputEmail" class="sr-only">Email address</label>
            <?php
            echo $form->field($model, 'username', [
                'inputOptions' => ['autofocus' => '', 'class' => 'form-control', 'id' => 'inputEmail']
            ])->textInput()->input('email', ['placeholder' => "Email"])->label(false);
            ?>

            <label for="inputPassword" class="sr-only">Password</label>

            <?php
            echo $form->field($model, 'password', [
                'inputOptions' => ['autofocus' => '', 'class' => 'form-control', 'id' => 'inputPassword']
            ])->textInput()->input('password', ['placeholder' => "Password"])->label(false);
            ?>

            <!--<label for="test1">Remember Me</label>-->				
             <br>
             <div class="row">
             <div class="col-xs-8">
            <?php echo $form->field($model, 'rememberMe', ['inputOptions' => ['id' => 'ttt']])->checkbox()->label('Remember Me'); ?>
            <a href="<?php echo Url::to(['site/forget']);?>">Forget password</a>
            </div>
            <div class="col-xs-4">
            <?= Html::submitButton('Sign in', ['class' => 'btn btn-lg btn-primary btn-block', 'name' => 'login-button']) ?>
            </div>
            </div>
            <?php ActiveForm::end(); ?>


        </div>

    </div>
</div>

