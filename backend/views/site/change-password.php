<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;


$this->title = 'Change Password';
?>

<section class="content-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </section>

<section class="content">

<div class="col-md-8">
<div class="box box-info">
    <?php 
            if((isset($data['status'])) && ($data['status'] == 'Changed'))
            {
            ?>
             <div class="alert alert-success">
            <strong>Your password has been successfully changed.</strong>
            </div>    
            <?php
            }
            elseif((isset($data['status'])) && ($data['status'] == 'Error'))
            {
            ?>
            <div class="alert alert-danger">
            <strong><?php echo $data['error'];?></strong>
            </div> 
            <?php }?>
            
            <!-- form start -->
            <?php $form = ActiveForm::begin(['action' => 'change-password', 'options' => ['class' => 'form-signin']]); ?> 
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Old Password</label>

                  <div class="col-sm-10">
                    <?= $form->field($model, 'oldPassword')->textInput()->input('password', ['placeholder' => "Old Password", 'class' => 'form-control', 'id' => 'oldPassword'])->label(false); ?>
                 </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">New Password</label>

                  <div class="col-sm-10">
                    <?= $form->field($model, 'newPassword')->textInput()->input('password', ['placeholder' => "New Password", 'class' => 'form-control', 'id' => 'newPassword'])->label(false); ?>
                 </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Confirm Password</label>

                  <div class="col-sm-10">
                    <?= $form->field($model, 'reNewPassword')->textInput()->input('password', ['placeholder' => "Confirm Password", 'class' => 'form-control', 'id' => 'reNewPassword'])->label(false); ?>
                 </div>
                </div>

              </div>
              <!-- /.box-body -->
              <div class="box-footer">
              <div class="col-xs-3">
                 <?= Html::submitButton('Submit', ['class' => 'btn btn-lg btn-primary btn-block', 'name' => 'login-button', 'id' => 'changeSubmit']) ?>
              </div>
              </div>
              <!-- /.box-footer -->
            <?php ActiveForm::end(); ?>
            
          </div>
          </div>

 
 </section>