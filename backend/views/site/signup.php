<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
?>

<div class="col-xs-12 col-sm-10 col-sm-push-1 col-md-10 col-md-push-1 col-lg-push-1 col-lg-10">
    <div class="signup_box">
        <div class="signin_logo text-center">
            <img src="<?php echo Url::to('@web/images/signin-logo.png'); ?>" alt="Signin Logo"/>
        </div>
        <div class="signup_inner_box">

            <?php $form = ActiveForm::begin(['action' => '', 'options' => ['class' => 'form-signin']]); ?>   
            <div class="row">
                <div class="col-xs-6">
                    <a class="btn btn-block btn-social btn-facebook">
                        <i class="fa fa-facebook"></i> Sign in with Facebook
                    </a>
                </div>
                <div class="col-xs-6">
                    <a class="btn btn-block btn-social btn-google">
                        <i class="fa fa-google"></i> Sign in with Google
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="or_seprator text-center">
                        <span>Or</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 form-group">
                    <label for="inputFullName" class="sr-only">Full Name</label>
                    <?php
                    echo $form->field($model, 'userName', [
                        'inputOptions' => ['autofocus' => '', 'class' => 'form-control', 'id' => 'inputFullName']
                    ])->textInput()->input('text', ['placeholder' => "Full Name"])->label(false);
                    ?>

                </div>

                <div class="col-xs-6 form-group">
                    <label for="inputCity" class="sr-only">City</label>
                    <input id="inputCity" class="form-control" placeholder="Country"  type="text">

                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 form-group">
                    <label for="inputEmail" class="sr-only">Email</label>

                    <?php
                    echo $form->field($model, 'userEmail', [
                        'inputOptions' => ['autofocus' => '', 'class' => 'form-control', 'id' => 'inputEmail']
                    ])->textInput()->input('email', ['placeholder' => "Email"])->label(false);
                    ?>
                </div>
                <div class="col-xs-6 form-group">
                    <label for="city" class="sr-only">City</label>


<?php
echo $form->field($model, 'city', [
    'inputOptions' => ['autofocus' => '', 'class' => 'form-control', 'id' => 'city']
])->textInput()->input('text', ['placeholder' => "City"])->label(false);
?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 form-group">
                    <label for="password" class="sr-only">Password</label>
                    <?php
                    echo $form->field($model, 'password', [
                        'inputOptions' => ['autofocus' => '', 'class' => 'form-control']
                    ])->textInput()->input('password', ['placeholder' => "Password"])->label(false);
                    ?>
                </div>
                <div class="col-xs-6 form-group">
                    <label for="cpassword" class="sr-only">Confirm Password</label>

<?php
echo $form->field($model, 'cpassword', [
    'inputOptions' => ['autofocus' => '', 'class' => 'form-control']
])->textInput()->input('password', ['placeholder' => "Confirm Password"])->label(false);
?>
                </div>
            </div>
            <div class="row">

                <div class="col-xs-6 form-group">
                    <label for="inputPhone" class="sr-only">Phone</label>

<?php
echo $form->field($model, 'userNumber', [
    'inputOptions' => ['autofocus' => '', 'class' => 'form-control', 'id' => 'inputPhone']
])->textInput()->input('text', ['placeholder' => "Phone"])->label(false);
?>
                </div>
                <div class="col-xs-6 form-group">
                    <label for="inputAddress" class="sr-only">Address</label>

<?php
echo $form->field($model, 'address', [
    'inputOptions' => ['autofocus' => '', 'class' => 'form-control', 'id' => 'inputAddress']
])->textInput()->input('text', ['placeholder' => "Address"])->label(false);
?>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 form-group">
                    <div class="checkbox_box">


<?= $form->field($model, 'subscribeNews', ['inputOptions' => ['id' => 'test1']])->checkbox()->label(false); ?>
                        <label for="test1">Subscribe Newsletter</label>				
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    <p>
                        By signing up, you agree to our <a href="#">T&C </a> and <a href="#">Privacy Policy</a>		
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 col-xs-push-3 form-group">
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
                </div>
            </div>
<?php ActiveForm::end(); ?>
        </div>

    </div>
</div>
