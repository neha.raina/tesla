<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\AboutUs */

$this->title = 'About Us';
// $this->params['breadcrumbs'][] = ['label' => 'About uses', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </section>

    <section class="content">
    <div class="col-md-6">
        <?= Html::a('Update', ['update', 'id' => $model->pkAboutUsID], [
            'class' => 'btn btn-primary',
        ]) ?>
    </div>
    <?php
$header = (isset($model->header)) ? $model->header : '';
$description = (isset($model->description)) ? $model->description : '';
$phoneNumber = (isset($model->phoneNumber)) ? $model->phoneNumber : '';
$startingRate = (isset($model->startingRate)) ? $model->startingRate : '';
$pricePerKm = (isset($model->pricePerKm)) ? $model->pricePerKm : '';
?>

   <div class="row">
    <div class="col-md-10 viewCat">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">About Us Information</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped catView">
                <tbody>
                <tr>
                  <th>Header</th>
                  <td><?php echo $header;?></td>
                </tr>
                <tr>
                  <th>Description</th>
                  <td><?php echo $description;?></td>
                </tr>
                <tr>
                  <th>Phone Number</th>
                  <td><?php echo $phoneNumber;?></td>
                </tr>
                <tr>
                  <th>Starting Rate</th>
                  <td><span class="adInfo"><?php echo $startingRate;?></span></td>
                </tr>
                <tr>
                  <th>Price per km</th>
                  <td><span class="positionInfo"><?php echo $pricePerKm;?></span></td>
                </tr>
              </tbody></table>
            </div>
          </div>
          </div>
</div>

