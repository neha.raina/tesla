<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\AboutUs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="col-md-10">
<div class="box box-info">
<div class="box-body">

    <?php $form = ActiveForm::begin(); ?>
    <div class="form-group">
      <label for="inputEmail3" class="col-sm-2 control-label">Header</label>

      <div class="col-sm-10">
        <?= $form->field($model, 'header')->textInput()->input('text', ['placeholder' => "Header", 'class' => 'form-control'])->label(false); ?>
     </div>
    </div>    
    <div class="form-group">
      <label for="inputEmail3" class="col-sm-2 control-label">Description</label>

      <div class="col-sm-10">
        <?= $form->field($model, 'description')->textarea(['rows' => '6','placeholder' => "Description", 'class' => 'form-control'])->label(false); ?>
     </div>
    </div>
    <div class="form-group">
      <label for="inputEmail3" class="col-sm-2 control-label">Phone Number</label>

      <div class="col-sm-10">
        <?= $form->field($model, 'phoneNumber')->textInput()->input('text', ['placeholder' => "Phone Number", 'class' => 'form-control'])->label(false); ?>
     </div>
    </div>
    <div class="form-group">
      <label for="inputEmail3" class="col-sm-2 control-label">Starting Rate</label>

      <div class="col-sm-10">
        <?= $form->field($model, 'startingRate')->textInput()->input('text', ['placeholder' => "Starting Rate", 'class' => 'form-control'])->label(false); ?>
     </div>
    </div>
    <div class="form-group">
      <label for="inputEmail3" class="col-sm-2 control-label">Price per km</label>

      <div class="col-sm-10">
        <?= $form->field($model, 'pricePerKm')->textInput()->input('text', ['placeholder' => "Price per km", 'class' => 'form-control'])->label(false); ?>
     </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
</div>
          </div>
