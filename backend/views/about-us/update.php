<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\AboutUs */

$this->title = 'Update About Us';
// $this->params['breadcrumbs'][] = ['label' => 'About uses', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->pkAboutUsID, 'url' => ['view', 'id' => $model->pkAboutUsID]];
// $this->params['breadcrumbs'][] = 'Update';
?>
<section class="content-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </section>

<section class="content">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
 </section>
