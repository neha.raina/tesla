<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchReservation */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'All Reservations';
//$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </section>
    <section class="content">

<?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="col-md-12">
    <div class="box">
    <div class="box-body">

      <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'Ad Title',
                'value' => function($dataProvider){ 
                    //echo '<pre>'; print_r($dataProvider); echo '</pre>'; die();
                      $url = Url::to(['ad/view']);
                      $finalUrl = $url.'?id='.$dataProvider->fkAd->pkAdID;
                      
                      $adTitle = $dataProvider->fkAd->title;
                      $adSubTitle = $dataProvider->fkAd->subTitle;
                      return '<u><a href="'.$finalUrl.'">'.$adTitle.'('.$adSubTitle.')'.'</a></u>';
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'Category Name',
                'value' => function($dataProvider){
                    //echo '<pre>'; print_r($dataProvider); echo '</pre>'; die();
                      $catName = $dataProvider->fkCategory->categoryName;
                      return $catName;
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'Users Name',
                'value' => function($dataProvider){
                      $url = Url::to(['reservation/view']);
                      $finalUrl = $url.'?id='.$dataProvider->pkReservationID;
                      return '<u><a href="'.$finalUrl.'">'.$dataProvider->userName.'</a></u>';
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'Phone No.',
                'value' => 'phoneNumber',
                'format' => 'html'
            ],
            [
                'attribute' => 'No. of people',
                'value' => function($dataProvider){ 
                      $people = $dataProvider->person;
                      return '<span class="positionInfo">'.$people.'</span>';
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'Booking Date',
                'value' => function($dataProvider){
                    //echo '<pre>'; print_r($dataProvider); echo '</pre>'; die();
                      $date = trim($dataProvider->bookingDate);
                      $currDate = date('Y-m-d');
                      if(($date == $currDate) || ($date < $currDate))
                      {
                        $color = '#dd4b39';
                      }else{
                        $color = '#00a65a';
                      }
                      $newDate = date("d F Y", strtotime($date));
                      return '<span style="color:'.$color.'">'.$newDate.'</span>';
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'Time',
                'value' => function($dataProvider){ 
                      return '<span class="adInfo">'.$dataProvider->bookingTime.'</span>';
                },
                'format' => 'html'
            ],
            // 'timestamp',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view}{delete}'],
        ],
    ]); ?>
      
    </div>
    </div>
    </div>


</section>
