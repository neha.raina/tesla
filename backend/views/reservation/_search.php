<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Ad;

/* @var $this yii\web\View */
/* @var $model backend\models\AdSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="col-md-12 searchCat">
<div class="box">
<div class="box-header">
<h3>Search</h3>
</div>

<div class="box-body">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
    <div class="col-lg-3 col-sm-4">
        <?php
        $items = ArrayHelper::map(Ad::find()->where(['dinning' => '1'])->joinWith(['categoryAdOrders'])->orderBy(['categoryAdOrder.position' => SORT_ASC])->all(), 'pkAdID', 'title');
              
        echo $form->field($model, 'fkAdID')->dropdownList($items,
        ['prompt'=>'Select Ad'])->label(false);
        ?>
         </div>
         <div class="col-lg-3 col-sm-4">
<?php echo $form->field($model, 'subTitle')->textInput()->input('text', ['placeholder' => "Ad Sub Title"])->label(false); ?>
         </div>
         <div class="col-lg-3 col-sm-4">
<?php echo $form->field($model, 'fromDate')->textInput()->input('text', ['placeholder' => "Booking From",'id' => 'datepickerFrom'])->label(false); ?>

         </div>
         <div class="col-lg-3 col-sm-4">
<?php echo $form->field($model, 'toDate')->textInput()->input('text', ['placeholder' => "Booking To",'id' => 'datepickerTo'])->label(false); ?>
         </div>
         <div class="col-lg-3 col-sm-4">
<?php echo $form->field($model, 'userName')->textInput()->input('text', ['placeholder' => "Users Name"])->label(false); ?>
         </div>
         <div class="col-lg-3 col-sm-4">
<?php echo $form->field($model, 'phoneNumber')->textInput()->input('text', ['placeholder' => "Phone Number"])->label(false); ?>
         </div>
         <div class="col-lg-3 col-sm-4">
<?php echo $form->field($model, 'person')->textInput()->input('text', ['placeholder' => "No. of People"])->label(false); ?>
         </div>
    <div class="col-lg-3 col-sm-4">
        <?= Html::submitButton('Search', ['class' => 'btn btn-info']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    </div>
</div>
</div>
</div>

    <?php // echo $form->field($model, 'bookingDate') ?>
