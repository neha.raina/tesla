<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Reservation */

$this->title = 'Update Reservation: ' . $model->pkReservationID;
$this->params['breadcrumbs'][] = ['label' => 'Reservations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pkReservationID, 'url' => ['view', 'id' => $model->pkReservationID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="reservation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
