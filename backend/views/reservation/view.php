<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Reservation */

$this->title = 'View Reservation';
// $this->params['breadcrumbs'][] = ['label' => 'Reservations', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </section>

    <section class="content">
    <div class="col-md-6">
        <?= Html::a('Delete', ['delete', 'id' => $model->pkReservationID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this category?',
                'method' => 'post',
            ],
        ]) ?>
        </div>
<?php
    //echo '<pre>'; print_r($model); echo '</pre>'; die;

$pkReservationID = (isset($model->pkReservationID)) ? $model->pkReservationID : '';
$categoryName = (isset($model->fkCategory->categoryName)) ? $model->fkCategory->categoryName : '';

$active = (isset($model->fkAd->active)) ? $model->fkAd->active : '';
$title = (isset($model->fkAd->title)) ? $model->fkAd->title : '';
$subTitle = (isset($model->fkAd->subTitle)) ? $model->fkAd->subTitle : '';
$address = (isset($model->fkAd->address)) ? $model->fkAd->address : '';
$streetName = (isset($model->fkAd->streetName)) ? $model->fkAd->streetName : '';
$cityName = (isset($model->fkAd->cityName)) ? $model->fkAd->cityName : '';
$countryName = (isset($model->fkAd->countryName)) ? $model->fkAd->countryName : '';
$contactNumber = (isset($model->fkAd->contactNumber)) ? $model->fkAd->contactNumber : '';

$finalAddress = $address.' '.$streetName.' '.$cityName.' '.$countryName;

$userName = (isset($model->userName)) ? $model->userName : '';
$userEmail = (isset($model->userEmail)) ? $model->userEmail : '';
$phoneNumber = (isset($model->phoneNumber)) ? $model->phoneNumber : '';
$bookingDate = (isset($model->bookingDate)) ? $model->bookingDate : '';
$bookingTime = (isset($model->bookingTime)) ? $model->bookingTime : '';
$person = (isset($model->person)) ? $model->person : '';
$timestamp = (isset($model->timestamp)) ? $model->timestamp : '';
?>
<div class="row">
    <div class="col-md-8 viewCat">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Restaurant Information</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped catView">
                <tbody>
                <tr>
                  <th>Restaurant title</th>
                  <td><span class="position-info"><?php echo $title;?></span></td>
                </tr>
                <tr>
                  <th>Sub-title</th>
                  <td><span class="adInfo"><?php echo $subTitle;?></span></td>
                </tr>
                <tr>
                  <th>Category Name</th>
                  <td><?php echo $categoryName;?></td>
                </tr>
                <tr>
                  <th>Active</th>
                  <td><?php if($active == '1'){?>
                    <i class="fa fa-check" aria-hidden="true"></i>
                   <?php }else{?>
                   <i class="fa fa-times" aria-hidden="true"></i>
                   <?php }?></td>
                </tr>
                <tr>
                  <th>Address</th>
                  <td><?php echo $finalAddress;?></td>
                </tr>
                <tr>
                  <th>Contact Number</th>
                  <td><?php echo $contactNumber;?></td>
                </tr>
                <tr>
                  <th>Date & Time of booking</th>
                  <td><?php 
                  echo gmdate("j F Y, g:i a", $timestamp);?></td>
                </tr>
              </tbody></table>
            </div>
          </div>

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">User Information</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped catView">
                <tbody>
                <tr>
                  <th>User Name</th>
                  <td><span class="adInfo"><?php echo $userName;?></span></td>
                </tr>
                <tr>
                  <th>Email</th>
                  <td><?php echo $userEmail;?></td>
                </tr>
                <tr>
                  <th>Contact Number</th>
                  <td><?php echo $phoneNumber;?></td>
                </tr>
                <tr>
                  <th>No. of people</th>
                  <td><?php echo $person;?></td>
                </tr>
                  
                <tr>
                  <th>Booking date & time</th>
                  <td><?php 
                      $date = trim($bookingDate);
                      $currDate = date('Y-m-d');
                      if(($date == $currDate) || ($date < $currDate))
                      {
                        $color = '#dd4b39';
                      }else{
                        $color = '#00a65a';
                      }
                      $newDate = date("d F Y", strtotime($date));
                      echo '<b><span style="color:'.$color.'">'.$newDate.', '.$bookingTime.'</span></b>';
                  ?></td>
                </tr>
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
          </div>
          </div>

 </section>
