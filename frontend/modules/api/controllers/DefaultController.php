<?php

namespace frontend\modules\api\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use frontend\models\Api;
use frontend\modules\api\models\App;


/**
 * Request Handler Controller Class
 * 
 */
class DefaultController extends Controller
{
    public $request;

    /**
     * @inheritdoc
     * Accept only Post Requests
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['post']
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     * Standalone action for error
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    // public $enableCsrfValidation = false;
    /**
     * Perform this action before every request
     * Date modified : Jul 13 2016
     */
    public function beforeAction($event)
    {

       /**
         * Snippet to check if a request method (GET/POST)
         * on an action is allowed or not
         */
        $action = $event->id;
        
        if(isset($this->actions[$action]))
        {
            $verbs = $this->actions[$action];
        }
        elseif(isset($this->actions['*']))
        {
            $verbs = $this->actions['*'];
        }
        else
        {
        }
        
        $verb = Yii::$app->getRequest()->getMethod();
        $allowed = array_map('strtoupper', $verbs);
        
        if(!in_array($verb, $allowed))
        {
            $response = Yii::$app->response;
            $response->format = \yii\web\Response::FORMAT_JSON;
            $response->data = ['code' => 201,'message' => 'Request method not allowed'];
            return;
        }



        /**
         * Set default request parser to json
         * Parse every request as Json
         * Set the parsed request to $request property
         */
        
        $this->request = Json::decode(Yii::$app->request->post('json_data'), false);   
        

        /**
         * Send all requests in Json format
         * Set Content Type as Json
         */
        Yii::$app->response->format = 'json';


        /**
         * Default Csrf protection on API Module
         */
        $this->enableCsrfValidation = false;
        return parent::beforeAction($event);
    }

    public function actionIndex()
    {
        
       //$request = Yii::$app->request->post('jsonData'); // $_POST['jsonData']
       // $jsonData = json_decode( $request, true );
        $data = array();

        $method = $this->request->method;
        switch( $method ){
            /**
             * Accounts and Profile related WebServices
             * @author Saurabh Sharma
             * @package Tafoos
             */
            
            case 'initialData':
            $data = App::initialData($this->request);
            break;
            
            case 'homeData':
            $data = App::homeData($this->request);
            break;

            case 'addDestination':
            $data = App::addDestination($this->request);
            break;

            case 'makeReservation':
            $data = App::makeReservation($this->request);
            break;

            case 'feedback':
            $data = App::feedback($this->request);
            break;
            
            

            


        }

        return $data;
    }
}
