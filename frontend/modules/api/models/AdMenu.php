<?php

namespace frontend\modules\api\models;

use Yii;

/**
 * This is the model class for table "ad_menu".
 *
 * @property string $pkAdMenuID
 * @property string $fkAdID
 * @property string $fkCategoryID
 * @property string $menuHeader
 * @property string $menuPdfPath
 *
 * @property Ad $fkAd
 * @property Category $fkCategory
 */
class AdMenu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ad_menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkAdID', 'fkCategoryID'], 'integer'],
            [['menuHeader'], 'string', 'max' => 500],
            [['menuPdfPath'], 'string', 'max' => 100],
            [['fkAdID'], 'exist', 'skipOnError' => true, 'targetClass' => Ad::className(), 'targetAttribute' => ['fkAdID' => 'pkAdID']],
            [['fkCategoryID'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['fkCategoryID' => 'pkCategoryID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkAdMenuID' => 'Pk Ad Menu ID',
            'fkAdID' => 'Fk Ad ID',
            'fkCategoryID' => 'Fk Category ID',
            'menuHeader' => 'Menu Header',
            'menuPdfPath' => 'Menu Pdf Path',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkAd()
    {
        return $this->hasOne(Ad::className(), ['pkAdID' => 'fkAdID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkCategory()
    {
        return $this->hasOne(Category::className(), ['pkCategoryID' => 'fkCategoryID']);
    }
}
