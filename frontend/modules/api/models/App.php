<?php

namespace frontend\modules\api\models;


use Yii;
use yii\base\Model;
use yii\db\Expression;
use yii\helpers\Url;

use frontend\modules\api\models\AboutUs;
use frontend\modules\api\models\Ad;
use frontend\modules\api\models\AdImage;
use frontend\modules\api\models\AdMenu;
use frontend\modules\api\models\Admin;
use frontend\modules\api\models\Category;
use frontend\modules\api\models\CategoryAdOrder;
use frontend\modules\api\models\Feedback;
use frontend\modules\api\models\Reservation;
use frontend\modules\api\models\UserDestination;



class App extends Model
{

    public static $errorMessage;
    public static $errorCode;

    /*****************************************************************
     *          Account and Profile Related WebServices
     ****************************************************************/
    
    /**/
    public static function initialData($request)
    {
        
        $data = array();
        $deviceToken = (isset($request->deviceToken)) ? $request->deviceToken : NULL;
        $latitude = (isset($request->latitude)) ? $request->latitude : NULL;
        $longitude = (isset($request->longitude)) ? $request->longitude : NULL;
        
        $categories = Category::find()->where(['active' => '1'])->orderby('position')->all();
        $categoryList = array();
        foreach ($categories as $category)
        {
            
            $categoryId = $category->pkCategoryID;
            $categoryName = $category->categoryName;
            
            $categoryList[] = array(
                    'id' => $categoryId,
                    'name' => $categoryName,
                    'dinning' => $category->dinning
                    );
        }

        $aboutUs = AboutUs::find()->all();
        $initialData = array(
            'headerName' => $aboutUs[0]->header,
            'description' => $aboutUs[0]->description,
            'startingRate' => $aboutUs[0]->startingRate,
            'pricePerKm' => $aboutUs[0]->pricePerKm,
            'contactNum' => $aboutUs[0]->phoneNumber,
                );
        
        $dataList = array();

        $currDate = date('Y-m-d h:i:s');
        
        /* this is for 1st add that is splash screen */
        $ads = Ad::find()->select(['*', 'categoryAdOrder.position'])->joinWith(['fkCategory','categoryAdOrders','adImages'])
        ->where(['category.active' => '1',  'ad.active' => '1'])
        ->andWhere(["<=","concat(startDate,' ',startDateTime)" ,$currDate])
        ->andWhere([">=","concat(endDate,' ',endDateTime)" ,$currDate])
        ->andWhere(['or', ['ad_image.imageType' => 'mainImage'], ['ad_image.imageType' => 'splashImage']])
        ->orderby(['category.position' => SORT_ASC, 'categoryAdOrder.position' => SORT_ASC])
        ->offset('0')
        ->limit('1')
        ->all();

        if($ads)
        {
            $baseUrl = Url::base(true);
            $myBaseUrl = str_replace("frontend","backend",$baseUrl);
             
            foreach($ads[0]->adImages as $images)
            {   

                if($images->imageType == 'mainImage'){
                $imageUrl = $myBaseUrl.'/ads/'.$images->imagePath;
                $imagesArr['mainImage'] = $imageUrl;
                }else{ $imagesArr['mainImage'] = NULL; }
            
                if($images->imageType == 'splashImage'){
                    $imageUrl = $myBaseUrl.'/ads/'.$images->imagePath;
                    $imagesArr['splashImage'] = $imageUrl;
                }else{ $imagesArr['splashImage'] = NULL; }

                if($images->imageType == 'leftImage'){
                    $imageUrl = $myBaseUrl.'/ads/'.$images->imagePath;
                    $imagesArr['leftImage'] = $imageUrl;
                }else{ $imagesArr['leftImage'] = NULL; }

                if($images->imageType == 'rightImage'){
                    $imageUrl = $myBaseUrl.'/ads/'.$images->imagePath;
                    $imagesArr['rightImage'] = $imageUrl;
                }else{ $imagesArr['rightImage'] = NULL; }
            }

                if(isset($ads[0]->videoPath)){
                    $videoPath = $myBaseUrl.'/ads/videos/'.$ads[0]->videoPath;
                }else{ $videoPath = NULL; }

                if(isset($ads[0]->reservationImage)){
                    $reservationImage = $myBaseUrl.'/ads/reservation/'.$ads[0]->reservationImage;
                }else{ $reservationImage = NULL; }
                
                $dataList[0] = array(
                'id' => $ads[0]->pkAdID,
                'categoryId' => $ads[0]->fkCategory->pkCategoryID,
                'categoryName' => $ads[0]->fkCategory->categoryName,
                'dinning' => $ads[0]->dinning,
                'displayType' => '0',
                'closeToDestination' => NULL,
                'splashImage' => $imagesArr['splashImage'],
                'mainImage' => $imagesArr['mainImage'],
                'imageLeft' => $imagesArr['leftImage'],
                'imageRight' => $imagesArr['rightImage'],
                'title' => $ads[0]->title,
                'subTitle' => $ads[0]->subTitle,
                'header' => $ads[0]->header,
                'description' => $ads[0]->description,
                'address' => $ads[0]->address,
                'streetName' => $ads[0]->streetName,
                'cityName' => $ads[0]->cityName,
                'countryName' => $ads[0]->countryName,
                'latitude' => $ads[0]->latitude,
                'longitude' => $ads[0]->longitude,
                'imageWidth' => $ads[0]->mainImageWidth,
                'imageHeight' => $ads[0]->mainImageHeight,
                'contactNumber' => $ads[0]->contactNumber,
                'videoPath' => $videoPath,
                'reservationImage' => $reservationImage,
                'reservationMessage' => $ads[0]->reservationMessage,
                'timestamp' => $ads[0]->timestamp,
                ); 

        }
        else
        {
                /* this is for 1st add that is splash screen and no splash screen timing was there*/
                $ads = Ad::find()->joinWith(['fkCategory','categoryAdOrders','adImages'])
                ->where(['category.active' => '1',  'ad.active' => '1'])
                ->andWhere(['or', ['ad_image.imageType' => 'mainImage'], ['ad_image.imageType' => 'splashImage']])
                ->orderby(['category.position' => SORT_ASC, 'categoryAdOrder.position' => SORT_ASC])
                ->offset('0')
                ->limit('1')
                ->all();

            if($ads)
            {
                
                $baseUrl = Url::base(true);
                $myBaseUrl = str_replace("frontend","backend",$baseUrl);
                 
                foreach($ads[0]->adImages as $images)
                {   

                    if($images->imageType == 'mainImage'){
                    $imageUrl = $myBaseUrl.'/ads/'.$images->imagePath;
                    $imagesArr['mainImage'] = $imageUrl;
                    }else{ $imagesArr['mainImage'] = NULL; }
                
                    if($images->imageType == 'splashImage'){
                        $imageUrl = $myBaseUrl.'/ads/'.$images->imagePath;
                        $imagesArr['splashImage'] = $imageUrl;
                    }else{ $imagesArr['splashImage'] = NULL; }

                    if($images->imageType == 'leftImage'){
                        $imageUrl = $myBaseUrl.'/ads/'.$images->imagePath;
                        $imagesArr['leftImage'] = $imageUrl;
                    }else{ $imagesArr['leftImage'] = NULL; }

                    if($images->imageType == 'rightImage'){
                        $imageUrl = $myBaseUrl.'/ads/'.$images->imagePath;
                        $imagesArr['rightImage'] = $imageUrl;
                    }else{ $imagesArr['rightImage'] = NULL; }
                }

                    if(isset($ads[0]->videoPath)){
                        $videoPath = $myBaseUrl.'/ads/videos/'.$ads[0]->videoPath;
                    }else{ $videoPath = NULL; }

                    if(isset($ads[0]->reservationImage)){
                        $reservationImage = $myBaseUrl.'/ads/reservation/'.$ads[0]->reservationImage;
                    }else{ $reservationImage = NULL; }
                    
                    $dataList[0] = array(
                    'id' => $ads[0]->pkAdID,
                    'categoryId' => $ads[0]->fkCategory->pkCategoryID,
                    'categoryName' => $ads[0]->fkCategory->categoryName,
                    'dinning' => $ads[0]->dinning,
                    'displayType' => '0',
                    'closeToDestination' => NULL,
                    'splashImage' => $imagesArr['splashImage'],
                    'mainImage' => $imagesArr['mainImage'],
                    'imageLeft' => $imagesArr['leftImage'],
                    'imageRight' => $imagesArr['rightImage'],
                    'title' => $ads[0]->title,
                    'subTitle' => $ads[0]->subTitle,
                    'header' => $ads[0]->header,
                    'description' => $ads[0]->description,
                    'address' => $ads[0]->address,
                    'streetName' => $ads[0]->streetName,
                    'cityName' => $ads[0]->cityName,
                    'countryName' => $ads[0]->countryName,
                    'latitude' => $ads[0]->latitude,
                    'longitude' => $ads[0]->longitude,
                    'imageWidth' => $ads[0]->mainImageWidth,
                    'imageHeight' => $ads[0]->mainImageHeight,
                    'contactNumber' => $ads[0]->contactNumber,
                    'videoPath' => $videoPath,
                    'reservationImage' => $reservationImage,
                    'reservationMessage' => $ads[0]->reservationMessage,
                    'timestamp' => $ads[0]->timestamp,
                    );             

            }
            else
            {

                $data['code'] = 500;
                $data['message'] = 'No add found with main image or splash image.';

                return $data;
            }
        

        }
            /*this is for map only */
                    $dataList[1] = array(
                    'id' => NULL,
                    'categoryId' => NULL,
                    'categoryName' => NULL,
                    'dinning' => NULL,
                    'displayType' => '2',
                    'closeToDestination' => NULL,
                    'splashImage' => NULL,
                    'mainImage' => NULL,
                    'imageLeft' => NULL,
                    'imageRight' => NULL,
                    'title' => NULL,
                    'subTitle' => NULL,
                    'header' => NULL,
                    'description' => NULL,
                    'address' => NULL,
                    'streetName' => NULL,
                    'cityName' => NULL,
                    'countryName' => NULL,
                    'latitude' => NULL,
                    'longitude' => NULL,
                    'imageWidth' => NULL,
                    'imageHeight' => NULL,
                    'contactNumber' => NULL,
                    'videoPath' => NULL,
                    'reservationImage' => NULL,
                    'reservationMessage' => NULL,
                    'timestamp' => NULL,
                    );


            $secondAd = Ad::find()->select(['*', 'categoryAdOrder.position'])->joinWith(['fkCategory','categoryAdOrders','adImages'])
            ->where(['category.active' => '1',  'ad.active' => '1'])
            ->andWhere(['not', ['ad.pkAdID' => $ads[0]->pkAdID]])
            ->andWhere(['ad_image.imageType' => 'mainImage'])
            ->andWhere(['>', 'ad.mainImageWidth', 'ad.mainImageHeight'])
            ->orderby(['category.position' => SORT_ASC, 'categoryAdOrder.position' => SORT_ASC])
            ->offset('0')
            ->limit('1')
            ->all();

            
            if($secondAd)
            {
                
                $baseUrl = Url::base(true);
                $myBaseUrl = str_replace("frontend","backend",$baseUrl);
                 
                foreach($secondAd[0]->adImages as $images)
                {   

                    if($images->imageType == 'mainImage'){
                    $imageUrl = $myBaseUrl.'/ads/'.$images->imagePath;
                    $imagesArr['mainImage'] = $imageUrl;
                    }else{ $imagesArr['mainImage'] = NULL; }
                
                    if($images->imageType == 'splashImage'){
                        $imageUrl = $myBaseUrl.'/ads/'.$images->imagePath;
                        $imagesArr['splashImage'] = $imageUrl;
                    }else{ $imagesArr['splashImage'] = NULL; }

                    if($images->imageType == 'leftImage'){
                        $imageUrl = $myBaseUrl.'/ads/'.$images->imagePath;
                        $imagesArr['leftImage'] = $imageUrl;
                    }else{ $imagesArr['leftImage'] = NULL; }

                    if($images->imageType == 'rightImage'){
                        $imageUrl = $myBaseUrl.'/ads/'.$images->imagePath;
                        $imagesArr['rightImage'] = $imageUrl;
                    }else{ $imagesArr['rightImage'] = NULL; }
                }

                    if(isset($secondAd[0]->videoPath)){
                        $videoPath = $myBaseUrl.'/ads/videos/'.$secondAd[0]->videoPath;
                    }else{ $videoPath = NULL; }

                    if(isset($secondAd[0]->reservationImage)){
                        $reservationImage = $myBaseUrl.'/ads/reservation/'.$secondAd[0]->reservationImage;
                    }else{ $reservationImage = NULL; }
                    
                    $dataList[2] = array(
                    'id' => $secondAd[0]->pkAdID,
                    'categoryId' => $secondAd[0]->fkCategory->pkCategoryID,
                    'categoryName' => $secondAd[0]->fkCategory->categoryName,
                    'dinning' => $secondAd[0]->dinning,
                    'displayType' => '1',
                    'closeToDestination' => NULL,
                    'splashImage' => $imagesArr['splashImage'],
                    'mainImage' => $imagesArr['mainImage'],
                    'imageLeft' => $imagesArr['leftImage'],
                    'imageRight' => $imagesArr['rightImage'],
                    'title' => $secondAd[0]->title,
                    'subTitle' => $secondAd[0]->subTitle,
                    'header' => $secondAd[0]->header,
                    'description' => $secondAd[0]->description,
                    'address' => $secondAd[0]->address,
                    'streetName' => $secondAd[0]->streetName,
                    'cityName' => $secondAd[0]->cityName,
                    'countryName' => $secondAd[0]->countryName,
                    'latitude' => $secondAd[0]->latitude,
                    'longitude' => $secondAd[0]->longitude,
                    'imageWidth' => $secondAd[0]->mainImageWidth,
                    'imageHeight' => $secondAd[0]->mainImageHeight,
                    'contactNumber' => $secondAd[0]->contactNumber,
                    'videoPath' => $videoPath,
                    'reservationImage' => $reservationImage,
                    'reservationMessage' => $secondAd[0]->reservationMessage,
                    'timestamp' => $secondAd[0]->timestamp,
                    );


            $restLandscapeAds = Ad::find()->select(['*', 'categoryAdOrder.position'])->joinWith(['fkCategory','categoryAdOrders','adImages'])
            ->where(['category.active' => '1',  'ad.active' => '1'])
            ->andWhere(['not', ['ad.pkAdID' => $ads[0]->pkAdID]])
            ->andWhere(['not', ['ad.pkAdID' => $secondAd[0]->pkAdID]])
            ->andWhere(['ad_image.imageType' => 'mainImage'])
            ->andWhere(['>', 'ad.mainImageWidth', 'ad.mainImageHeight'])
            ->orderby(['category.position' => SORT_ASC, 'categoryAdOrder.position' => SORT_ASC])
            ->all();

            $restPortraitAds = Ad::find()->select(['*', 'categoryAdOrder.position'])->joinWith(['fkCategory','categoryAdOrders','adImages'])
            ->where(['category.active' => '1',  'ad.active' => '1'])
            ->andWhere(['not', ['ad.pkAdID' => $ads[0]->pkAdID]])
            ->andWhere(['not', ['ad.pkAdID' => $secondAd[0]->pkAdID]])
            ->andWhere(['ad_image.imageType' => 'mainImage'])
            ->andWhere(['>', 'ad.mainImageHeight', 'ad.mainImageWidth'])
            ->orderby(['category.position' => SORT_ASC, 'categoryAdOrder.position' => SORT_ASC])
            ->all();

            $format = "hh,f,hm";
            $formated = explode(',', $format);
            $index = -1;
            
            while($restLandscapeAds || $restPortraitAds)
            {
                switch($formated[++$index % 3]) {

                    case 'hh':
                          if(count($restLandscapeAds) >= 2) {
                            $dataList[] = self::assignData($restLandscapeAds, 1);
                            $dataList[] = self::assignData($restLandscapeAds, 1);
                          }
                        break;

                    case 'f':
                          if(count($restPortraitAds) >= 1)
                            $dataList[] = self::assignData($restPortraitAds, 0);
                        break;

                    case 'hm':
                          if(count($restLandscapeAds) == 1 && count($restPortraitAds) == 0) {

                            $dataList[] = self::assignData($restLandscapeAds, 1);
                            $dataList[] = self::assignMap($restLandscapeAds, 1);
            
                            // $dataList[] = self::assignData($restLandscapeAds, 1);
                            //$dataList[] = self::assignMap();
                            break;
                          }
                        break;

                }
    
            }

                $data['code'] = 200;
                $data['message'] = 'Success';
                $data['categoryList'] = $categoryList;
                $data['initialData'] = $initialData;
                $data['dataList'] = $dataList;
            
            return $data;


            }
            else
            {
                $data['code'] = 500;
                $data['message'] = 'No add found with main image landscape.';

                return $data;
            }
            
    }

    public static function homeData($request)
    {
        
        $data = array();
        $latitude = (isset($request->latitude)) ? $request->latitude : NULL;
        $longitude = (isset($request->longitude)) ? $request->longitude : NULL;
        
        $dataList = array();

        $currDate = date('Y-m-d h:i:s');
        
        /* this is for 1st add that is splash screen */
        $ads = Ad::find()->select(['*', 'categoryAdOrder.position'])->joinWith(['fkCategory','categoryAdOrders','adImages'])
        ->where(['category.active' => '1',  'ad.active' => '1'])
        ->andWhere(["<=","concat(startDate,' ',startDateTime)" ,$currDate])
        ->andWhere([">=","concat(endDate,' ',endDateTime)" ,$currDate])
        ->andWhere(['or', ['ad_image.imageType' => 'mainImage'], ['ad_image.imageType' => 'splashImage']])
        ->orderby(['category.position' => SORT_ASC, 'categoryAdOrder.position' => SORT_ASC])
        ->offset('0')
        ->limit('1')
        ->all();

        if($ads)
        {
            $baseUrl = Url::base(true);
            $myBaseUrl = str_replace("frontend","backend",$baseUrl);
             
            foreach($ads[0]->adImages as $images)
            {   

                if($images->imageType == 'mainImage'){
                $imageUrl = $myBaseUrl.'/ads/'.$images->imagePath;
                $imagesArr['mainImage'] = $imageUrl;
                }else{ $imagesArr['mainImage'] = NULL; }
            
                if($images->imageType == 'splashImage'){
                    $imageUrl = $myBaseUrl.'/ads/'.$images->imagePath;
                    $imagesArr['splashImage'] = $imageUrl;
                }else{ $imagesArr['splashImage'] = NULL; }

                if($images->imageType == 'leftImage'){
                    $imageUrl = $myBaseUrl.'/ads/'.$images->imagePath;
                    $imagesArr['leftImage'] = $imageUrl;
                }else{ $imagesArr['leftImage'] = NULL; }

                if($images->imageType == 'rightImage'){
                    $imageUrl = $myBaseUrl.'/ads/'.$images->imagePath;
                    $imagesArr['rightImage'] = $imageUrl;
                }else{ $imagesArr['rightImage'] = NULL; }
            }

                if(isset($ads[0]->videoPath)){
                    $videoPath = $myBaseUrl.'/ads/videos/'.$ads[0]->videoPath;
                }else{ $videoPath = NULL; }

                if(isset($ads[0]->reservationImage)){
                    $reservationImage = $myBaseUrl.'/ads/reservation/'.$ads[0]->reservationImage;
                }else{ $reservationImage = NULL; }
                
                $dataList[0] = array(
                'id' => $ads[0]->pkAdID,
                'categoryId' => $ads[0]->fkCategory->pkCategoryID,
                'categoryName' => $ads[0]->fkCategory->categoryName,
                'dinning' => $ads[0]->dinning,
                'displayType' => '0',
                'closeToDestination' => NULL,
                'splashImage' => $imagesArr['splashImage'],
                'mainImage' => $imagesArr['mainImage'],
                'imageLeft' => $imagesArr['leftImage'],
                'imageRight' => $imagesArr['rightImage'],
                'title' => $ads[0]->title,
                'subTitle' => $ads[0]->subTitle,
                'header' => $ads[0]->header,
                'description' => $ads[0]->description,
                'address' => $ads[0]->address,
                'streetName' => $ads[0]->streetName,
                'cityName' => $ads[0]->cityName,
                'countryName' => $ads[0]->countryName,
                'latitude' => $ads[0]->latitude,
                'longitude' => $ads[0]->longitude,
                'imageWidth' => $ads[0]->mainImageWidth,
                'imageHeight' => $ads[0]->mainImageHeight,
                'contactNumber' => $ads[0]->contactNumber,
                'videoPath' => $videoPath,
                'reservationImage' => $reservationImage,
                'reservationMessage' => $ads[0]->reservationMessage,
                'timestamp' => $ads[0]->timestamp,
                ); 

        }
        else
        {
                /* this is for 1st add that is splash screen and no splash screen timing was there*/
                $ads = Ad::find()->joinWith(['fkCategory','categoryAdOrders','adImages'])
                ->where(['category.active' => '1',  'ad.active' => '1'])
                ->andWhere(['or', ['ad_image.imageType' => 'mainImage'], ['ad_image.imageType' => 'splashImage']])
                ->orderby(['category.position' => SORT_ASC, 'categoryAdOrder.position' => SORT_ASC])
                ->offset('0')
                ->limit('1')
                ->all();

            if($ads)
            {
                
                $baseUrl = Url::base(true);
                $myBaseUrl = str_replace("frontend","backend",$baseUrl);
                 
                foreach($ads[0]->adImages as $images)
                {   

                    if($images->imageType == 'mainImage'){
                    $imageUrl = $myBaseUrl.'/ads/'.$images->imagePath;
                    $imagesArr['mainImage'] = $imageUrl;
                    }else{ $imagesArr['mainImage'] = NULL; }
                
                    if($images->imageType == 'splashImage'){
                        $imageUrl = $myBaseUrl.'/ads/'.$images->imagePath;
                        $imagesArr['splashImage'] = $imageUrl;
                    }else{ $imagesArr['splashImage'] = NULL; }

                    if($images->imageType == 'leftImage'){
                        $imageUrl = $myBaseUrl.'/ads/'.$images->imagePath;
                        $imagesArr['leftImage'] = $imageUrl;
                    }else{ $imagesArr['leftImage'] = NULL; }

                    if($images->imageType == 'rightImage'){
                        $imageUrl = $myBaseUrl.'/ads/'.$images->imagePath;
                        $imagesArr['rightImage'] = $imageUrl;
                    }else{ $imagesArr['rightImage'] = NULL; }
                }

                    if(isset($ads[0]->videoPath)){
                        $videoPath = $myBaseUrl.'/ads/videos/'.$ads[0]->videoPath;
                    }else{ $videoPath = NULL; }

                    if(isset($ads[0]->reservationImage)){
                        $reservationImage = $myBaseUrl.'/ads/reservation/'.$ads[0]->reservationImage;
                    }else{ $reservationImage = NULL; }
                    
                    $dataList[0] = array(
                    'id' => $ads[0]->pkAdID,
                    'categoryId' => $ads[0]->fkCategory->pkCategoryID,
                    'categoryName' => $ads[0]->fkCategory->categoryName,
                    'dinning' => $ads[0]->dinning,
                    'displayType' => '0',
                    'closeToDestination' => NULL,
                    'splashImage' => $imagesArr['splashImage'],
                    'mainImage' => $imagesArr['mainImage'],
                    'imageLeft' => $imagesArr['leftImage'],
                    'imageRight' => $imagesArr['rightImage'],
                    'title' => $ads[0]->title,
                    'subTitle' => $ads[0]->subTitle,
                    'header' => $ads[0]->header,
                    'description' => $ads[0]->description,
                    'address' => $ads[0]->address,
                    'streetName' => $ads[0]->streetName,
                    'cityName' => $ads[0]->cityName,
                    'countryName' => $ads[0]->countryName,
                    'latitude' => $ads[0]->latitude,
                    'longitude' => $ads[0]->longitude,
                    'imageWidth' => $ads[0]->mainImageWidth,
                    'imageHeight' => $ads[0]->mainImageHeight,
                    'contactNumber' => $ads[0]->contactNumber,
                    'videoPath' => $videoPath,
                    'reservationImage' => $reservationImage,
                    'reservationMessage' => $ads[0]->reservationMessage,
                    'timestamp' => $ads[0]->timestamp,
                    );             

            }
            else
            {

                $data['code'] = 500;
                $data['message'] = 'No add found with main image or splash image.';

                return $data;
            }
        

        }
            /*this is for map only */
                    $dataList[1] = array(
                    'id' => NULL,
                    'categoryId' => NULL,
                    'categoryName' => NULL,
                    'dinning' => NULL,
                    'displayType' => '2',
                    'closeToDestination' => NULL,
                    'splashImage' => NULL,
                    'mainImage' => NULL,
                    'imageLeft' => NULL,
                    'imageRight' => NULL,
                    'title' => NULL,
                    'subTitle' => NULL,
                    'header' => NULL,
                    'description' => NULL,
                    'address' => NULL,
                    'streetName' => NULL,
                    'cityName' => NULL,
                    'countryName' => NULL,
                    'latitude' => NULL,
                    'longitude' => NULL,
                    'imageWidth' => NULL,
                    'imageHeight' => NULL,
                    'contactNumber' => NULL,
                    'videoPath' => NULL,
                    'reservationImage' => NULL,
                    'reservationMessage' => NULL,
                    'timestamp' => NULL,
                    );


            $secondAd = Ad::find()->select(['*', 'categoryAdOrder.position'])->joinWith(['fkCategory','categoryAdOrders','adImages'])
            ->where(['category.active' => '1',  'ad.active' => '1'])
            ->andWhere(['not', ['ad.pkAdID' => $ads[0]->pkAdID]])
            ->andWhere(['ad_image.imageType' => 'mainImage'])
            ->andWhere(['>', 'ad.mainImageWidth', 'ad.mainImageHeight'])
            ->orderby(['category.position' => SORT_ASC, 'categoryAdOrder.position' => SORT_ASC])
            ->offset('0')
            ->limit('1')
            ->all();

            
            if($secondAd)
            {
                
                $baseUrl = Url::base(true);
                $myBaseUrl = str_replace("frontend","backend",$baseUrl);
                 
                foreach($secondAd[0]->adImages as $images)
                {   

                    if($images->imageType == 'mainImage'){
                    $imageUrl = $myBaseUrl.'/ads/'.$images->imagePath;
                    $imagesArr['mainImage'] = $imageUrl;
                    }else{ $imagesArr['mainImage'] = NULL; }
                
                    if($images->imageType == 'splashImage'){
                        $imageUrl = $myBaseUrl.'/ads/'.$images->imagePath;
                        $imagesArr['splashImage'] = $imageUrl;
                    }else{ $imagesArr['splashImage'] = NULL; }

                    if($images->imageType == 'leftImage'){
                        $imageUrl = $myBaseUrl.'/ads/'.$images->imagePath;
                        $imagesArr['leftImage'] = $imageUrl;
                    }else{ $imagesArr['leftImage'] = NULL; }

                    if($images->imageType == 'rightImage'){
                        $imageUrl = $myBaseUrl.'/ads/'.$images->imagePath;
                        $imagesArr['rightImage'] = $imageUrl;
                    }else{ $imagesArr['rightImage'] = NULL; }
                }

                    if(isset($secondAd[0]->videoPath)){
                        $videoPath = $myBaseUrl.'/ads/videos/'.$secondAd[0]->videoPath;
                    }else{ $videoPath = NULL; }

                    if(isset($secondAd[0]->reservationImage)){
                        $reservationImage = $myBaseUrl.'/ads/reservation/'.$secondAd[0]->reservationImage;
                    }else{ $reservationImage = NULL; }
                    
                    $dataList[2] = array(
                    'id' => $secondAd[0]->pkAdID,
                    'categoryId' => $secondAd[0]->fkCategory->pkCategoryID,
                    'categoryName' => $secondAd[0]->fkCategory->categoryName,
                    'dinning' => $secondAd[0]->dinning,
                    'displayType' => '1',
                    'closeToDestination' => NULL,
                    'splashImage' => $imagesArr['splashImage'],
                    'mainImage' => $imagesArr['mainImage'],
                    'imageLeft' => $imagesArr['leftImage'],
                    'imageRight' => $imagesArr['rightImage'],
                    'title' => $secondAd[0]->title,
                    'subTitle' => $secondAd[0]->subTitle,
                    'header' => $secondAd[0]->header,
                    'description' => $secondAd[0]->description,
                    'address' => $secondAd[0]->address,
                    'streetName' => $secondAd[0]->streetName,
                    'cityName' => $secondAd[0]->cityName,
                    'countryName' => $secondAd[0]->countryName,
                    'latitude' => $secondAd[0]->latitude,
                    'longitude' => $secondAd[0]->longitude,
                    'imageWidth' => $secondAd[0]->mainImageWidth,
                    'imageHeight' => $secondAd[0]->mainImageHeight,
                    'contactNumber' => $secondAd[0]->contactNumber,
                    'videoPath' => $videoPath,
                    'reservationImage' => $reservationImage,
                    'reservationMessage' => $secondAd[0]->reservationMessage,
                    'timestamp' => $secondAd[0]->timestamp,
                    );


            $restLandscapeAds = Ad::find()->select(['*', 'categoryAdOrder.position'])->joinWith(['fkCategory','categoryAdOrders','adImages'])
            ->where(['category.active' => '1',  'ad.active' => '1'])
            ->andWhere(['not', ['ad.pkAdID' => $ads[0]->pkAdID]])
            ->andWhere(['not', ['ad.pkAdID' => $secondAd[0]->pkAdID]])
            ->andWhere(['ad_image.imageType' => 'mainImage'])
            ->andWhere(['>', 'ad.mainImageWidth', 'ad.mainImageHeight'])
            ->orderby(['category.position' => SORT_ASC, 'categoryAdOrder.position' => SORT_ASC])
            ->all();

            $restPortraitAds = Ad::find()->select(['*', 'categoryAdOrder.position'])->joinWith(['fkCategory','categoryAdOrders','adImages'])
            ->where(['category.active' => '1',  'ad.active' => '1'])
            ->andWhere(['not', ['ad.pkAdID' => $ads[0]->pkAdID]])
            ->andWhere(['not', ['ad.pkAdID' => $secondAd[0]->pkAdID]])
            ->andWhere(['ad_image.imageType' => 'mainImage'])
            ->andWhere(['>', 'ad.mainImageHeight', 'ad.mainImageWidth'])
            ->orderby(['category.position' => SORT_ASC, 'categoryAdOrder.position' => SORT_ASC])
            ->all();

            $format = "hh,f,hm";
            $formated = explode(',', $format);
            $index = -1;
            
            while($restLandscapeAds || $restPortraitAds)
            {
                switch($formated[++$index % 3]) {

                    case 'hh':
                          if(count($restLandscapeAds) >= 2) {
                            $dataList[] = self::assignData($restLandscapeAds, 1);
                            $dataList[] = self::assignData($restLandscapeAds, 1);
                          }
                        break;

                    case 'f':
                          if(count($restPortraitAds) >= 1)
                            $dataList[] = self::assignData($restPortraitAds, 0);
                        break;

                    case 'hm':
                          if(count($restLandscapeAds) == 1 && count($restPortraitAds) == 0) {

                            $dataList[] = self::assignData($restLandscapeAds, 1);
                            $dataList[] = self::assignMap($restLandscapeAds, 1);
            
                            // $dataList[] = self::assignData($restLandscapeAds, 1);
                            //$dataList[] = self::assignMap();
                            break;
                          }
                        break;

                }
    
            }

                $data['code'] = 200;
                $data['message'] = 'Success';
                $data['dataList'] = $dataList;
            
            return $data;


            }
            else
            {
                $data['code'] = 500;
                $data['message'] = 'No add found with main image landscape.';

                return $data;
            }
            
    }

    protected static function assignData(&$models, $displayType = 1)
    {
        $secondAd = array_pop($models);


        $baseUrl = Url::base(true);
        $myBaseUrl = str_replace("frontend","backend",$baseUrl);
         
        foreach($secondAd->adImages as $images)
        {   

            if($images->imageType == 'mainImage'){
            $imageUrl = $myBaseUrl.'/ads/'.$images->imagePath;
            $imagesArr['mainImage'] = $imageUrl;
            }else{ $imagesArr['mainImage'] = NULL; }
        
            if($images->imageType == 'splashImage'){
                $imageUrl = $myBaseUrl.'/ads/'.$images->imagePath;
                $imagesArr['splashImage'] = $imageUrl;
            }else{ $imagesArr['splashImage'] = NULL; }

            if($images->imageType == 'leftImage'){
                $imageUrl = $myBaseUrl.'/ads/'.$images->imagePath;
                $imagesArr['leftImage'] = $imageUrl;
            }else{ $imagesArr['leftImage'] = NULL; }

            if($images->imageType == 'rightImage'){
                $imageUrl = $myBaseUrl.'/ads/'.$images->imagePath;
                $imagesArr['rightImage'] = $imageUrl;
            }else{ $imagesArr['rightImage'] = NULL; }
        }

            if(isset($secondAd->videoPath)){
                $videoPath = $myBaseUrl.'/ads/videos/'.$secondAd->videoPath;
            }else{ $videoPath = NULL; }

            if(isset($secondAd->reservationImage)){
                $reservationImage = $myBaseUrl.'/ads/reservation/'.$secondAd->reservationImage;
            }else{ $reservationImage = NULL; }


        $dataList = array(
                    'id' => $secondAd->pkAdID,
                    'categoryId' => $secondAd->fkCategory->pkCategoryID,
                    'categoryName' => $secondAd->fkCategory->categoryName,
                    'dinning' => $secondAd->dinning,
                    'displayType' => $displayType,
                    'closeToDestination' => NULL,
                    'splashImage' => $imagesArr['splashImage'],
                    'mainImage' => $imagesArr['mainImage'],
                    'imageLeft' => $imagesArr['leftImage'],
                    'imageRight' => $imagesArr['rightImage'],
                    'title' => $secondAd->title,
                    'subTitle' => $secondAd->subTitle,
                    'header' => $secondAd->header,
                    'description' => $secondAd->description,
                    'address' => $secondAd->address,
                    'streetName' => $secondAd->streetName,
                    'cityName' => $secondAd->cityName,
                    'countryName' => $secondAd->countryName,
                    'latitude' => $secondAd->latitude,
                    'longitude' => $secondAd->longitude,
                    'imageWidth' => $secondAd->mainImageWidth,
                    'imageHeight' => $secondAd->mainImageHeight,
                    'contactNumber' => $secondAd->contactNumber,
                    'videoPath' => $videoPath,
                    'reservationImage' => $reservationImage,
                    'reservationMessage' => $secondAd->reservationMessage,
                    'timestamp' => $secondAd->timestamp,
        );

        return $dataList;


    }

    protected static function assignMap()
    {
        $dataList = array(
                    'id' => NULL,
                    'categoryId' => NULL,
                    'categoryName' => NULL,
                    'dinning' => NULL,
                    'displayType' => '2',
                    'closeToDestination' => NULL,
                    'splashImage' => NULL,
                    'mainImage' => NULL,
                    'imageLeft' => NULL,
                    'imageRight' => NULL,
                    'title' => NULL,
                    'subTitle' => NULL,
                    'header' => NULL,
                    'description' => NULL,
                    'address' => NULL,
                    'streetName' => NULL,
                    'cityName' => NULL,
                    'countryName' => NULL,
                    'latitude' => NULL,
                    'longitude' => NULL,
                    'imageWidth' => NULL,
                    'imageHeight' => NULL,
                    'contactNumber' => NULL,
                    'videoPath' => NULL,
                    'reservationImage' => NULL,
                    'reservationMessage' => NULL,
                    'timestamp' => NULL,
                    );

        return $dataList;
    }


    public static function addDestination($request)
    {
        $data = array();
        $latitude = (isset($request->latitude)) ? $request->latitude : NULL;
        $longitude = (isset($request->longitude)) ? $request->longitude : NULL;
        $cityName = (isset($request->cityName)) ? $request->cityName : NULL;
        $streetName = (isset($request->streetName)) ? $request->streetName : NULL;
        $userNumber = (isset($request->userNumber)) ? $request->userNumber : NULL;

        $currTime = time();

        $addDestination = new UserDestination();
        $addDestination->streetName = $streetName;
        $addDestination->cityName = $cityName;
        $addDestination->phoneNumber = $userNumber;
        $addDestination->latitude = $latitude;
        $addDestination->longitude = $longitude;
        $addDestination->timestamp = $currTime;

        if($addDestination->save())
        {
          
            $data['code'] = 200;
            $data['status'] = 'Success';

        }
        else
        {
            $data['code'] = 500;
            $data['message'] = 'Error Occurred, Please try again later.';
        }

        return $data;

    }

    public static function makeReservation($request)
    {
        $data = array();
        $adId = (isset($request->adId)) ? $request->adId : NULL;
        $userName = (isset($request->userName)) ? $request->userName : NULL;
        $userEmailId = (isset($request->userEmailId)) ? $request->userEmailId : NULL;
        $userNumber = (isset($request->userNumber)) ? $request->userNumber : NULL;
        $date = (isset($request->date)) ? $request->date : NULL;
        $numOfPeoples = (isset($request->numOfPeoples)) ? $request->numOfPeoples : NULL;
        $time = (isset($request->time)) ? $request->time : NULL;

        $currTime = time();

        $adInfo = Ad::find()->select('fkCategoryID')->where(['pkAdID' => $adId])->one();
        $categoryId = (isset($adInfo->fkCategoryID)) ? $adInfo->fkCategoryID : NULL;

        $reservation = new Reservation();
        $reservation->fkAdID = $adId;
        $reservation->fkCategoryID = $categoryId;
        $reservation->confirmation = ' 1';
        $reservation->userName = $userName;
        $reservation->userEmail = $userEmailId;
        $reservation->phoneNumber = $userNumber;
        $reservation->bookingDate = $date;
        $reservation->bookingTime = $time;
        $reservation->person = $numOfPeoples;
        $reservation->timestamp = $currTime;

        if($reservation->save())
        {

            $res = 'Hi '.$userName.', Thankyou for make the resevation. Your reservation date is '.$date.' and time is '.$time;
            mail($userEmailId,'Cabitall', $res);
          
            $data['code'] = 200;
            $data['status'] = 'Success';

        }
        else
        {
            $data['code'] = 500;
            $data['message'] = 'Error Occurred, Please try again later.';
        }

        return $data;

    }

    public static function feedback($request)
    {
        $data = array();
        $name = (isset($request->name)) ? $request->name : NULL;
        $userEmail = (isset($request->userEmail)) ? $request->userEmail : NULL;
        $message = (isset($request->message)) ? $request->message : NULL;
        $latitude = (isset($request->latitude)) ? $request->latitude : NULL;
        $longitude = (isset($request->longitude)) ? $request->longitude : NULL;
        

        $currTime = time();

        $addFeedback = new Feedback();
        $addFeedback->name = $name;
        $addFeedback->emailID = $userEmail;
        $addFeedback->message = $message;
        $addFeedback->latitude = $latitude;
        $addFeedback->longitude = $longitude;
        $addFeedback->timestamp = $currTime;

        if($addFeedback->save())
        {
          
            $data['code'] = 200;
            $data['status'] = 'Success';

        }
        else
        {
            $data['code'] = 500;
            $data['message'] = 'Error Occurred, Please try again later.';
        }

        return $data;

    }
    

} 

//http://maps.googleapis.com/maps/api/distancematrix/json?origins=28.6810032,77.0853157&destinations=29.9456906,78.1642478&mode=driving&language=en-EN&sensor=false