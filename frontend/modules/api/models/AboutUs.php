<?php

namespace frontend\modules\api\models;

use Yii;

/**
 * This is the model class for table "about_us".
 *
 * @property string $pkAboutUsID
 * @property string $header
 * @property string $description
 * @property string $phoneNumber
 * @property string $startingRate
 * @property string $pricePerKm
 */
class AboutUs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'about_us';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['header', 'description'], 'string', 'max' => 1000],
            [['phoneNumber'], 'string', 'max' => 20],
            [['startingRate'], 'string', 'max' => 10],
            [['pricePerKm'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkAboutUsID' => 'Pk About Us ID',
            'header' => 'Header',
            'description' => 'Description',
            'phoneNumber' => 'Phone Number',
            'startingRate' => 'Starting Rate',
            'pricePerKm' => 'Price Per Km',
        ];
    }
}
