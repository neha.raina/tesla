<?php

namespace frontend\modules\api\models;

use Yii;

/**
 * This is the model class for table "categoryAdOrder".
 *
 * @property string $pkCategoryAdOrder
 * @property string $fkAdID
 * @property string $fkCategoryID
 * @property integer $position
 *
 * @property Ad $fkAd
 * @property Category $fkCategory
 */
class CategoryAdOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categoryAdOrder';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkAdID', 'fkCategoryID', 'position'], 'integer'],
            [['fkAdID'], 'exist', 'skipOnError' => true, 'targetClass' => Ad::className(), 'targetAttribute' => ['fkAdID' => 'pkAdID']],
            [['fkCategoryID'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['fkCategoryID' => 'pkCategoryID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkCategoryAdOrder' => 'Pk Category Ad Order',
            'fkAdID' => 'Fk Ad ID',
            'fkCategoryID' => 'Fk Category ID',
            'position' => 'Position',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkAd()
    {
        return $this->hasOne(Ad::className(), ['pkAdID' => 'fkAdID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkCategory()
    {
        return $this->hasOne(Category::className(), ['pkCategoryID' => 'fkCategoryID']);
    }
}
