<?php

namespace frontend\modules\api\models;

use Yii;

/**
 * This is the model class for table "ad".
 *
 * @property string $pkAdID
 * @property string $fkCategoryID
 * @property integer $active
 * @property integer $dinning
 * @property string $title
 * @property string $subTitle
 * @property string $header
 * @property string $description
 * @property string $address
 * @property string $streetName
 * @property string $cityName
 * @property string $countryName
 * @property string $latitude
 * @property string $longitude
 * @property string $contactNumber
 * @property string $videoPath
 * @property string $reservationImage
 * @property string $reservationMessage
 * @property string $startDate
 * @property string $startDateTime
 * @property string $endDate
 * @property string $endDateTime
 * @property string $timestamp
 *
 * @property Category $fkCategory
 * @property AdImage[] $adImages
 * @property AdMenu[] $adMenus
 * @property CategoryAdOrder[] $categoryAdOrders
 * @property Reservation[] $reservations
 */
class Ad extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ad';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fkCategoryID', 'active', 'dinning'], 'integer'],
            [['title', 'subTitle', 'streetName', 'cityName', 'countryName', 'latitude', 'longitude'], 'string', 'max' => 100],
            [['header', 'videoPath', 'reservationImage'], 'string', 'max' => 200],
            [['description', 'address', 'reservationMessage'], 'string', 'max' => 1000],
            [['contactNumber', 'startDate', 'startDateTime', 'endDate', 'endDateTime'], 'string', 'max' => 20],
            [['timestamp'], 'string', 'max' => 12],
            [['fkCategoryID'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['fkCategoryID' => 'pkCategoryID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkAdID' => 'Pk Ad ID',
            'fkCategoryID' => 'Fk Category ID',
            'active' => 'Active',
            'dinning' => 'Dinning',
            'title' => 'Title',
            'subTitle' => 'Sub Title',
            'header' => 'Header',
            'description' => 'Description',
            'address' => 'Address',
            'streetName' => 'Street Name',
            'cityName' => 'City Name',
            'countryName' => 'Country Name',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'contactNumber' => 'Contact Number',
            'videoPath' => 'Video Path',
            'reservationImage' => 'Reservation Image',
            'reservationMessage' => 'Reservation Message',
            'startDate' => 'Start Date',
            'startDateTime' => 'Start Date Time',
            'endDate' => 'End Date',
            'endDateTime' => 'End Date Time',
            'timestamp' => 'Timestamp',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkCategory()
    {
        return $this->hasOne(Category::className(), ['pkCategoryID' => 'fkCategoryID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdImages()
    {
        return $this->hasMany(AdImage::className(), ['fkAdID' => 'pkAdID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdMenus()
    {
        return $this->hasMany(AdMenu::className(), ['fkAdID' => 'pkAdID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryAdOrders()
    {
        return $this->hasMany(CategoryAdOrder::className(), ['fkAdID' => 'pkAdID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReservations()
    {
        return $this->hasMany(Reservation::className(), ['fkAdID' => 'pkAdID']);
    }
}
