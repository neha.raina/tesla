<?php

namespace frontend\modules\api\models;

use Yii;

/**
 * This is the model class for table "user_destination".
 *
 * @property string $pkUserDestinationID
 * @property string $streetName
 * @property string $cityName
 * @property string $phoneNumber
 * @property string $latitude
 * @property string $longitude
 * @property string $timestamp
 */
class UserDestination extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_destination';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['streetName'], 'string', 'max' => 50],
            [['cityName'], 'string', 'max' => 30],
            [['phoneNumber', 'latitude', 'longitude'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkUserDestinationID' => 'Pk User Destination ID',
            'streetName' => 'Street Name',
            'cityName' => 'City Name',
            'phoneNumber' => 'Phone Number',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'timestamp' => 'Timestamp',
        ];
    }
}
