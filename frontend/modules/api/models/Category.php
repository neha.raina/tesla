<?php

namespace frontend\modules\api\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property string $pkCategoryID
 * @property string $categoryName
 * @property integer $dinning
 * @property integer $active
 * @property integer $position
 * @property string $videoPath
 * @property string $timestamp
 *
 * @property Ad[] $ads
 * @property AdImage[] $adImages
 * @property AdMenu[] $adMenus
 * @property CategoryAdOrder[] $categoryAdOrders
 * @property Reservation[] $reservations
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dinning', 'active', 'position', 'timestamp'], 'integer'],
            [['categoryName'], 'string', 'max' => 50],
            [['videoPath'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkCategoryID' => 'Pk Category ID',
            'categoryName' => 'Category Name',
            'dinning' => 'Dinning',
            'active' => 'Active',
            'position' => 'Position',
            'videoPath' => 'Video Path',
            'timestamp' => 'Timestamp',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAds()
    {
        return $this->hasMany(Ad::className(), ['fkCategoryID' => 'pkCategoryID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdImages()
    {
        return $this->hasMany(AdImage::className(), ['fkCategoryID' => 'pkCategoryID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdMenus()
    {
        return $this->hasMany(AdMenu::className(), ['fkCategoryID' => 'pkCategoryID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryAdOrders()
    {
        return $this->hasMany(CategoryAdOrder::className(), ['fkCategoryID' => 'pkCategoryID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReservations()
    {
        return $this->hasMany(Reservation::className(), ['fkCategoryID' => 'pkCategoryID']);
    }
}
