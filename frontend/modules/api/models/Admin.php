<?php

namespace frontend\modules\api\models;

use Yii;

/**
 * This is the model class for table "admin".
 *
 * @property integer $pkAdminID
 * @property string $adminEmail
 * @property string $adminPassword
 * @property string $adminResetToken
 * @property string $auth_key
 */
class Admin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'admin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['adminEmail', 'adminResetToken', 'auth_key'], 'string', 'max' => 100],
            [['adminPassword'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pkAdminID' => 'Pk Admin ID',
            'adminEmail' => 'Admin Email',
            'adminPassword' => 'Admin Password',
            'adminResetToken' => 'Admin Reset Token',
            'auth_key' => 'Auth Key',
        ];
    }
}
