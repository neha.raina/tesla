<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
	'formatter' => [
        	'defaultTimeZone' => 'Asia/Kolkata'
       	],
	'gcm' => [
            'class' => 'bryglen\apnsgcm\Gcm',
            'apiKey' => 'AIzaSyBSCaIhQiYPZq24go_z0QHsEgWAxJcScXM',
        ],
	
    ],
];